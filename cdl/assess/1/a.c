#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <string.h>

#define TKL 33

typedef struct token {
    int type;
    char lexeme[80];
} Token;

typedef struct symb {
    int no;
    int arg;
    char name[80];
    int type;
    int rtype;
} symb;

symb *tab;
int tb_i = 0;

enum tokenTypes {IDENT, INT, PLUS, SUB, MUL, DIV, EQ, LP, RP, LCB, RCB, SEMI_COLON, LT, LTE, GT, GTE, EQEQ, PEQ, SEQ, MEQ, DEQ, MOD, MDEQ, QSTN, COLON, STRINGC, CHARC, INTC, COMMA, ASSIGN, DOT, CLASS, STRING};

char *tokenTypesStr[] = {"ident", "int", "+", "-", "*", "/", "=", "(", ")", "{", "}", ";", "<", "<=", ">", ">=", "==", "+=", "-=", "*=", "/=", "%", "%=", "?", ":", "stringc", "charc", "intc", ",", "<-", ".", "class", "String"};

char *tokenNames[] = {"ident", "int", "plus", "sub", "mul", "div", "eq", "lp", "rp", "lcb", "rcb", "sc", "lt", "lte", "gt", "gte", "eqeq", "peq", "seq", "meq", "deq", "mod", "mdeq", "qstn", "colon", "stringc", "charc", "intc", "comma", "assign", "dot", "class", "String"};

int in(char *s, char *arr[])
{
    int i;
    for (i = 0; i < TKL; i++)
		if (strncmp(s, arr[i], 80) == 0)
			return i;
    return -1;
}

Token* getToken(FILE *fp)
{
    Token *token = (Token *) malloc(sizeof(Token));
    char c, n;
    int lexeme_idx = 0;

    int type_idx;
    
    while ((c = fgetc(fp)) != EOF) {
		// comment skip
		if (c == '-') {
		    while ((n = fgetc(fp)) != '\n' && c != EOF);
		    ungetc(n, fp);
		}

		// skip whitespace automatically

		if (c == '"') {
			token->type = STRINGC;
			token->lexeme[lexeme_idx++] = c;
			while ((c = fgetc(fp)) != '"' && c != EOF) {
				token->lexeme[lexeme_idx++] = c;
			}
			token->lexeme[lexeme_idx++] = c;
			return token;
		}
		// ident or numerical constants or invalid tokens
		else if (isalnum(c)) {
			token->lexeme[lexeme_idx++] = c;
			// numbers
			if (isdigit(c)) {
				token->type = INTC;
				while ((c = fgetc(fp)) && isdigit(c)) {
					token->lexeme[lexeme_idx++] = c;
				}
				ungetc(c, fp);
			}
			// idents and kewords
			else {
				while ((c = fgetc(fp)) && isalnum(c) || c == '_') {
					token->lexeme[lexeme_idx++] = c;
				}
				ungetc(c, fp);

				type_idx = in(token->lexeme, tokenTypesStr);
				token->type = type_idx == -1 ? IDENT : type_idx;
			}
	    
			return token;
		}
		// symbol tokens
		else if (!isspace(c)) {
			token->lexeme[lexeme_idx++] = c;
			token->type = in(token->lexeme, tokenTypesStr);
				// check if arithmatic symbol
				if (token->type == LT) {
					n = fgetc(fp);
					// check if combined operator
					if (n == '-') {
						token->lexeme[lexeme_idx++] = n;
						token->type = ASSIGN;
					}
					else {
						ungetc(n, fp);
					}
				}
				else {
			token->type = in(token->lexeme, tokenTypesStr);
				}
			return token;
		}
	} // while

    return NULL;
}

void tokenize(FILE *fp)
{
    int scope = G;
    Token *tk, *data_type_tk, *ptr, *var;

    while ((tk = getToken(fp)) != NULL) {
	printf("<%s,%s>\n", tokenTypesStr[tk->type], tk->lexeme); 

	if (tk->type == IDENT) {
	    // is tk in table

	    int i;
	    int fl = 0;
	    for (i = 0; i < tb_i; i++)
		if (strcmp(tk->lexeme, tab[i].name) == 0) {
		    fl = 1;
		    break;
		}

	    if (fl == 1)
		continue;

	    var = tk;

	    tk = getToken(fp);
	    printf("<%s,%s>\n", tokenTypesStr[tk->type], tk->lexeme); 
	    
	    if (tk->type == COLON) {
		// var
		data_type_tk = getToken(fp);
		printf("<%s,%s>\n", tokenTypesStr[tk->type], tk->lexeme); 

		tab[tb_i] = tb_i;
		tab[tb_i].type = tk->type;
		tab[tb_i].rtype = -1;
		strncpy(tab[tb_i].name, tk->lexeme, 80);
		tb_i++;
	    }

	    else if (tk->type == LP) {
		// function
		int fi = tb_i;

		tab[tb_i] = tb_i;
		tab[tb_i].type = FUNC;
		tab[tb_i].rtype = ;
		strncpy(tab[tb_i].name, tk->lexeme, 80);
		tab[fi].arg = tb_i;
		tb_i++;

		var = tk;
		// get args
		tk = getToken(fp);
		printf("<%s,%s>\n", tokenTypesStr[tk->type], tk->lexeme); 
	    
		data_type_tk = getToken(fp);
		printf("<%s,%s>\n", tokenTypesStr[tk->type], tk->lexeme); 

		tab[tb_i] = tb_i;
		tab[tb_i].type = tk->type;
		tab[tb_i].rtype = -1;
		strncpy(tab[tb_i].name, tk->lexeme, 80);
		tab[fi].arg = tb_i;
		tb_i++;
	    }
	} // if ident
    } // while 
} // tokenize

int main(void)
{
    char *filename = (char *) malloc(sizeof(char) * 80);
    fgets(filename, 79, stdin);

    int l = strlen(filename);
    filename[l-1] = '\0';

    FILE *fdi = fopen(filename, "r");

    int i;
    Token *tk;

    while ((tk = getToken(fdi)) != NULL)
	printf("<%s,%s>\n", tokenNames[tk->type], tk->lexeme); 

    fclose(fdi);

    return 0;
}
