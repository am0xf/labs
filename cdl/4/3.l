%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int pi = 0, ni = 0, pf = 0, nf = 0;

%}

INT [0-9]+
FLOAT [0-9]+(\.[0-9]+)?([eE][+-]?[0-9]+)?

%%
-{INT} { ni++; }
{INT} { pi++; }
-{FLOAT} { nf++; }
{FLOAT} { pf++; }
. {}
%%

int main(void)
{
    yylex();

    printf("PI : %d\nNI : %d\nPF : %d\nNF : %d\n", pi, ni, pf, nf);

    return 0;
}

int yywrap()
{
    return 1;
}

