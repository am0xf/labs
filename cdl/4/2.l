%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int w = 0, c = 0, b = 0, l = 0;
%}

%%
[a-zA-Z0-9]+ { c += strlen(yytext); w++;}
\n { c++; l++; b++; }
\t|" " { b++; }
. { c++; }
%%

int main(void)
{
    yylex();

    printf("W : %d\nC : %d\nB : %d\nL : %d\n", w, c, b, l);

    return 0;
}

int yywrap()
{
    return 1;
}

