%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int compound = 0;

%}

%%
[a-zA-Z]+" and "[a-zA-Z]+|[a-zA-Z]+" or "[a-zA-Z]+ { compound = 1; }
. {}
%%

int main(void)
{
    yylex();

    printf("%d\n", compound);

    return 0;
}

int yywrap()
{
    return 1;
}

