%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int v = 0, c = 0;
%}

%%
[aeiouAEIOU] { v++; }
[a-z] { c++; }
%%

int main(void)
{
    yylex();

    printf("V : %d\n C : %d\n", v, c);

    return 0;
}

int yywrap()
{
    return 1;
}

