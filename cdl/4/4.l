%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

FILE *fp;

%}

%%
"//".*$ { fputs(yytext, fp); }
"/*"(.|\n)*"*/" { fputs(yytext, fp); }
"\"".*"\"" { fputs(yytext, fp); }
"printf" { fputs("WRITE", fp); }
"scanf" { fputs("READ", fp); }
.|\n { fputs(yytext, fp); }
%%

int main(void)
{ 
    yyin = fopen("program.c", "r");
    fp = fopen("4.out", "w");
    yylex();

    fclose(fp);

    return 0;
}

int yywrap()
{
    return 1;
}

