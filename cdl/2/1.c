#include "lexer.h"

int main(void)
{
    char *filename = (char *) malloc(sizeof(char) * 80);
    fgets(filename, 79, stdin);

    int l = strlen(filename);
    filename[l-1] = '\0';

    FILE *fdi = fopen(filename, "r");

    int line = 1, col = 1, nos = 1;

    Token *tk;
    while ((tk = getToken(fdi, &line, &col)) != NULL) {
		printf("%d : <%s, %d, %d %s>\n", nos, tk->lexeme, tk->row, tk->col, tokenNames[tk->type]);
		nos++;
    }
    fclose(fdi);

    return 0;
}
