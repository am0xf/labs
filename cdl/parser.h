#include "symbol-table.h"

Token *tk = NULL;

int lp();
int rp();
int lsqb();
int rsqb();
int lcb();
int rcb();

int comma();
int semi_colon();

int iF();
int Else();

int mulop();
int addop();
int relop();

int eq();

int ident();
int id_extn();
int arr();
int id_extn2();
int id_extn3();
int id_list();

int num();
int factor();
int tprime();
int term();
int seprime();
int simple_expn();
int eprime();
int expn();
int assign_stat();
int looping_stat();
int dprime();
int decision_stat();
int decl_stat();
int stat();
int stat_list();

int Main()
{
    tk = tk == NULL ? getToken() : tk;
 
    if (tk->type == IDENT && strncmp("main", tk->lexeme, 4) == 0) {
		tk = NULL;
		return 1;
    }

    else {
		return 0;
    }
}

int ident()
{
    tk = tk == NULL ? getToken() : tk;
 
    if (tk->type == IDENT) {
		tk = NULL;
		return 1;
    }

    else {
		return 0;
    }
}

int id_extn3()
{
    //printf("id_extn3\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");
	return comma() ? id_list() : 1;
}

int arr()
{
    //printf("arr\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");

	if (lsqb()) {
		if (!num()) {
			printf("integer constant expected at line %d, col %d\n", tk->row, tk->col);
			printf("found %s\n", tk != NULL ? tk->lexeme : "");

			Token *cur = tk;
			while (!rsqb() && !semi_colon() && !rcb()) // follow of num
				cur = tk;

			tk = cur;
		}

		if (!rsqb()) {
			printf("] expected at line %d, col %d\n", tk->row, tk->col);
			printf("found %s\n", tk != NULL ? tk->lexeme : "");

			Token *cur = tk;
			while (!rsqb() && !semi_colon() && !rcb()) // follow of rsqb 
				cur = tk;

			tk = cur;
		}

		return 1;
	}

	else {
		return 1;
	}
}

int id_extn2()
{
    //printf("id_extn2\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");
    return arr() || 1;
}

int id_extn()
{
    //printf("id_extn\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");
    return id_extn2() && id_extn3();
}

int id_list()
{
    //printf("id_list\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");

	if (!ident()) {
		printf("identifier expected at line %d, col %d\n", tk->row, tk->col);
		printf("found %s\n", tk != NULL ? tk->lexeme : "");

		Token *cur = tk;
		while (!comma() && !semi_colon() && !rcb()) // follow of id
			cur = tk;

		tk = cur;
	}

    return id_extn();
}

int type()
{
    tk = tk == NULL ? getToken() : tk;

    switch (tk->type) {
		case CHAR:
		case DOUBLE:
		case FLOAT:
		case INT:
		case LONG:
		case SHORT:
		case VOID:
		    tk = NULL;
		    return 1;
		    
		default:
		    return 0;
    }
}

int decl_stat()
{
    //printf("decl_stat\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");

    return type() && id_list();
}

int eq()
{
    tk = tk == NULL ? getToken() : tk;
 
    if (tk->type == EQ) {
		tk = NULL;
		return 1;
    }

    else {
		return 0;
    }
}

int lp()
{
    tk = tk == NULL ? getToken() : tk;
 
    if (tk->type == LP) {
		tk = NULL;
		return 1;
    }

    else {
		return 0;
    }
}

int rp()
{
    tk = tk == NULL ? getToken() : tk;
 
    if (tk->type == RP) {
		tk = NULL;
		return 1;
    }

    else {
		return 0;
    }
}

int lcb()
{
    tk = tk == NULL ? getToken() : tk;
 
    if (tk->type == LCB) {
		tk = NULL;
		return 1;
    }

    else {
		return 0;
    }
}

int rcb()
{
    tk = tk == NULL ? getToken() : tk;
 
    if (tk->type == RCB) {
		tk = NULL;
		return 1;
    }

    else {
		return 0;
    }
}

int lsqb()
{
    tk = tk == NULL ? getToken() : tk;
 
    if (tk->type == LSQB) {
		tk = NULL;
		return 1;
    }

    else {
		return 0;
    }
}

int rsqb()
{
    tk = tk == NULL ? getToken() : tk;
 
    if (tk->type == RSQB) {
		tk = NULL;
		return 1;
    }

    else {
		return 0;
    }
}

int comma()
{
    tk = tk == NULL ? getToken() : tk;
 
    if (tk->type == COMMA) {
		tk = NULL;
		return 1;
    }

    else {
		return 0;
    }
}

int semi_colon()
{
    tk = tk == NULL ? getToken() : tk;
 
    if (tk->type == SEMI_COLON) {
		tk = NULL;
		return 1;
    }

    else {
		return 0;
    }
}

int If()
{
    tk = tk == NULL ? getToken() : tk;
 
    if (tk->type == IF) {
		tk = NULL;
		return 1;
    }

    else {
		return 0;
    }
}

int Else()
{
    tk = tk == NULL ? getToken() : tk;

    if (tk->type == ELSE) {
		tk = NULL;
		return 1;
    }

    else {
		return 0;
    }
}

int mulop()
{
    tk = tk == NULL ? getToken() : tk;

    switch (tk->type) {
		case MUL :
		case DIV :
		case MOD :
			tk = NULL;
			return 1;
	
		default:
			return 0;
		}
}

int addop()
{
    tk = tk == NULL ? getToken() : tk;

    switch (tk->type) {
		case PLUS:
		case SUB:
			tk = NULL;
			return 1;
	
		default:
			return 0;
	}
}

int relop()
{
    tk = tk == NULL ? getToken() : tk;

    switch (tk->type) {
		case EQEQ:
		case NEQ:
		case LT:
		case LTE:
		case GT:
		case GTE:
			tk = NULL;
			return 1;
	
		default:
			return 0;
    }
}

int num()
{
    tk = tk == NULL ? getToken() : tk;

    switch (tk->type) {
		case INTC:
		case SHORTC:
		case LONGC:
		case CHARC:
			tk = NULL;
			return 1;
	
		default:
			return 0;
    }
}

int factor()
{
    tk = tk == NULL ? getToken() : tk;

    switch (tk->type) {
		case IDENT:
		case INTC:
		case SHORTC:
		case LONGC:
		case FLOATC:
		case DOUBLEC:
		case CHARC:
			tk = NULL;
			return 1;

		default:
			return 0;
    }
}

int tprime()
{
    //printf("tprime\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");
    return mulop() ? term() : 1;
}

int term()
{
    //printf("term\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");

    if (factor()) {
		return tprime();
    }

    else {
		printf("constant or identifier expected at line %d, col %d\n", tk->row, tk->col);
		printf("found %s\n", tk != NULL ? tk->lexeme : "");

		Token *cur = tk;
		while (!semi_colon() && !rcb()) // skip full statement (use follow instead)
			cur = tk;

		tk = cur;
    }
}

int seprime()
{
    //printf("seprime\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");
    return addop() ? term() && seprime() : 1;
}

int simple_expn()
{
    //printf("simp_expn\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");
    return term() && seprime();
}

int eprime()
{
    //printf("eprime\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");

    return relop() ? simple_expn() : 1;
}

int expn()
{
    //printf("expn\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");
    return simple_expn() && eprime();
}

int assign_stat()
{
    //printf("assign_stat\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");

    if (ident()) {
		if (!eq()) {
			printf("= expected at line %d, col %d\n", tk->row, tk->col);
			printf("found %s\n", tk != NULL ? tk->lexeme : "");

			Token *cur = tk;
			while (!factor() && !lp() && !semi_colon() && !rcb()) // follow of eq
				cur = tk;

			tk = cur;
		}

		return expn();
	}

	else {
		return 0;
	}
		    
}

int looping_stat()
{
    tk = tk == NULL ? getToken() : tk;

    switch (tk->type) {

		case WHILE:
		    tk = NULL;
	
		    if (!lp()) {
				printf("( expected at line %d, col %d\n", tk->row, tk->col);
				printf("found %s\n", tk != NULL ? tk->lexeme : "");

				// don't skip input
			}
	
			if (expn()) {
			    if (!rp()) {
					printf(") expected at line %d, col %d\n", tk->row, tk->col);
					printf("found %s\n", tk != NULL ? tk->lexeme : "");

					// don't skip input
				}
	
				if (!lcb()) {
					printf("{ expected at line %d, col %d\n", tk->row, tk->col);
					printf("found %s\n", tk != NULL ? tk->lexeme : "");

					// don't skip input
				}
	
				stat_list();
	
				if (!rcb()) {
					printf("} expected at line %d, col %d\n", tk->row, tk->col);
					printf("found %s\n", tk != NULL ? tk->lexeme : "");

					// don't skip input
				}
	
				return 1;
			}
	
			else {
			    return 0;
			}
		
		case FOR:
		    tk = NULL;

		    if (!lp()) {
				printf("( expected at line %d, col %d\n", tk->row, tk->col);
				printf("found %s\n", tk != NULL ? tk->lexeme : "");
					
				// don't skip input
			}
	
			if (assign_stat()) {
				if (!semi_colon()) {
					printf("; expected at line %d, col %d\n", tk->row, tk->col);
					printf("found %s\n", tk != NULL ? tk->lexeme : "");
					printf("%d\n", __LINE__);	
					// don't skip input
				}

				if (expn()) {
					if (!semi_colon()) {
						printf("; expected at line %d, col %d\n", tk->row, tk->col);
						printf("found %s\n", tk != NULL ? tk->lexeme : "");
					printf("%d\n", __LINE__);	
						
						// don't skip input
					}

					if (assign_stat()) {
						if (!rp()) {
							printf(") expected at line %d, col %d\n", tk->row, tk->col);
							printf("found %s\n", tk != NULL ? tk->lexeme : "");
						
							// don't skip input
						}

						if (!lcb()) {
							printf("{ expected at line %d, col %d\n", tk->row, tk->col);
							printf("found %s\n", tk != NULL ? tk->lexeme : "");
							
							// don't skip input
						}

						stat_list();

						if (!rcb()) {
							printf("} expected at line %d, col %d\n", tk->row, tk->col);
							printf("found %s\n", tk != NULL ? tk->lexeme : "");
							
							// don't skip input
						}

						return 1;
					}
				}

				else {
					return 0;
				}
			}

			else {
				return 0;
			}

		default:
			return 0;
    }
}

int dprime()
{
    //printf("dprime\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");

	if (Else()) {
		if (!lcb()) {
			printf("( expected at line %d, col %d\n", tk->row, tk->col), 0;
			printf("found %s\n", tk != NULL ? tk->lexeme : "");
							
			// don't skip input
		}

		stat_list();

		if (!rcb()) {
			printf("( expected at line %d, col %d\n", tk->row, tk->col), 0;
			printf("found %s\n", tk != NULL ? tk->lexeme : "");

			// don't skip input
		}
	}

	else {
		return 1;
	}
}

int decision_stat()
{
    //printf("decision_stat\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");

    if (If()) {
		if (!lp()) {
		    printf("( expected at line %d, col %d\n", tk->row, tk->col), 0;
		    printf("found %s\n", tk != NULL ? tk->lexeme : "");

			// don't skip input
		}

		if (expn()) {
		    if (!rp()) {
				printf(") expected at line %d, col %d\n", tk->row, tk->col), 0;
				printf("found %s\n", tk != NULL ? tk->lexeme : "");
			
				// don't skip input
			}
	
		    if (!lcb()) {
				printf("{ expected at line %d, col %d\n", tk->row, tk->col), 0;
				printf("found %s\n", tk != NULL ? tk->lexeme : "");
				
				// don't skip input
			}

			stat_list();

			if (!rcb()) {
				printf("{ expected at line %d, col %d\n", tk->row, tk->col), 0;
				printf("found %s\n", tk != NULL ? tk->lexeme : "");
				
				// don't skip input
			}
	    
			return dprime();
		}

		else {
			return 0;
		}
	}

    else {
		return 0;
    }
}

int stat()
{
    //printf("stat\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");

    if (assign_stat()) {
		if (!semi_colon()) {
			printf("; expected at line %d, col %d\n", tk->row, tk->col), 0;
			printf("found %s\n", tk != NULL ? tk->lexeme : "");

			// don't skip input
		}
		
		return 1;
    }

    else if (decl_stat()) {
		if (!semi_colon()) {
			printf("; expected at line %d, col %d\n", tk->row, tk->col), 0;
			printf("found %s\n", tk != NULL ? tk->lexeme : "");
			
			// don't skip input
		}

		return 1;
    }

    else {
		return decision_stat() || looping_stat();
    }
}

int stat_list()
{
    //printf("stat_list\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");

    return stat() ? stat_list() : 1;
}

int program()
{
    //printf("program\n");
    //printf("%s\n", tk != NULL ? tk->lexeme : "");

    if (!Main()) {
		printf("main() expected at line %d, col %d\n", tk->row, tk->col), 0;
		printf("found %s\n", tk != NULL ? tk->lexeme : "");
		return 0;
    }

    if (!lp()) {
		printf("( expected at line %d, col %d\n", tk->row, tk->col), 0;
		printf("found %s\n", tk != NULL ? tk->lexeme : "");
			
		// don't skip input
    }

    if (!rp()) {
		printf(") expected at line %d, col %d\n", tk->row, tk->col), 0;
		printf("found %s\n", tk != NULL ? tk->lexeme : "");
		
		// don't skip input
    }

    if (!lcb()) {
		printf("} expected at line %d, col %d\n", tk->row, tk->col), 0;
		printf("found %s\n", tk != NULL ? tk->lexeme : "");
		
		// don't skip input
	}

    stat_list();

    if (!rcb()) {
		printf("} expected at line %d, col %d\n", tk->row, tk->col), 0;
		printf("found %s\n", tk != NULL ? tk->lexeme : "");
		
		// don't skip input
	}

    return 1;
}

