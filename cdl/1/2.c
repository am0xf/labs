#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
    char *filename = (char *) malloc(sizeof(char) * 80);
    char *line = (char *) malloc(sizeof(char) * 80);
    fgets(filename, 79, stdin);

    int l = strlen(filename);
    filename[l-1] = '\0';

    #define A 4

    FILE *fdi = fopen(filename, "r");
    FILE *fdo = fopen("outp2", "w");

    int c, i;
    int line_start = 0;

    while (fgets(line, 80, fdi) != NULL) {
	i = 0;

	#define H 4

	while (i < strlen(line) && line[i] == ' ' || line[i] == '\t')
	    i++;

	if (line[i] == '#')
	    continue;

	fputs(line, fdo);
    }

    fclose(fdi);
    fclose(fdo);

    return 0;
}

#define Y 4
//
