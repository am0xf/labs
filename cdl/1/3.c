#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define KWL 32

char *keywords[] = {"auto", "break", "case", "char", "const", "continue", "default", "do", "double", "else", "enum", "extern", "float", "for", "goto", "if", "int", "long", "register", "return", "short", "signed", "sizeof", "static", "struct", "switch", "typedef", "union", "unsigned", "void", "volatile", "while"};

int check_keyword(const char *s)
{
    int l = 0, r = KWL - 1, m = (r + l) / 2, d;

    while (l <= r) {
	m = (r + l) / 2;
	d = strncmp(s, keywords[m], 80);

	if (d < 0)
	    r = m - 1;
	else if (d > 0)
	    l = m + 1;
	else {
	    return 1;
	}
    }

    return 0;    
}

int main(void)
{
    char *filename = (char *) malloc(sizeof(char) * 80);
    fgets(filename, 79, stdin);

    int l = strlen(filename);
    filename[l-1] = '\0';

    FILE *fdi = fopen(filename, "r");

    int line = 1, col;
    int c, i;

    char *token_buffer = (char *) malloc(sizeof(char) * 80);
    int tk_bf_idx = 0, kcol;

    while ((c = fgetc(fdi)) != EOF) {
	if (isalpha(c)) {
	    if (islower(c)) {
		token_buffer[tk_bf_idx++] = c;
		kcol = col;
	    }
	    else
		tk_bf_idx = 0;
	}
	else {
	    token_buffer[tk_bf_idx] = '\0';

	    if (tk_bf_idx && check_keyword(token_buffer)) {
		printf("Line : %d Column : %d Keyword : ", line, kcol);

		for(i = 0; i < tk_bf_idx; i++)
		    token_buffer[i] = toupper(token_buffer[i]);

		token_buffer[tk_bf_idx] = '\n';
		token_buffer[tk_bf_idx+1] = '\0';
		fputs(token_buffer, stdout);
		tk_bf_idx = 0;

	    }
	    else
		tk_bf_idx = 0;

	    if (c == '\n') {
		line++;
		col = 1;
		tk_bf_idx = 0;
	    }
	}

	col++;
    }

    fclose(fdi);

    return 0;
}
