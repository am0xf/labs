#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
    char *filename = (char *) malloc(sizeof(char) * 80);
    char *line = (char *) malloc(sizeof(char) * 80);

    fgets(filename, 79, stdin);

    int l = strlen(filename);
    filename[l-1] = '\0';

    FILE *fdi = fopen(filename, "r");
    FILE *fdo = fopen("outpa1", "w");

    int lineno = 1;

    while (fgets(line, 80, fdi) != NULL) {
	fprintf(fdo, "%d ", lineno);
	fputs(line, fdo);
	lineno++;
    }	

    fclose(fdi);
    fclose(fdo);

    return 0;
}
