#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
    char *filename = (char *) malloc(sizeof(char) * 80);
    fgets(filename, 79, stdin);

    int l = strlen(filename);
    filename[l-1] = '\0';

    FILE *fdi = fopen(filename, "r");
    FILE *fdo = fopen("outp1", "w");

    int c;
    int in_space = 0;

    while ((c = fgetc(fdi)) != EOF) {
	if (c == ' ') {
	    if (in_space)
		continue;
	    else 
		in_space = 1;
	}
	else if (c == '\t')
	    c = ' ';
	else
	    in_space = 0;

	fputc(c, fdo);
    }

    fclose(fdi);
    fclose(fdo);

    return 0;
}

