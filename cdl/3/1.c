#include "symbol-table.h"

int main(void)
{
    char *filename = (char *) malloc(sizeof(char) * 80);
    fgets(filename, 79, stdin);

    int l = strlen(filename);
    filename[l-1] = '\0';

    FILE *fdi = fopen(filename, "r");

    int line = 1, col = 1, nos = 1;
	init_lexer(fdi, line, col);

    Token *tk;
    while ((tk = getToken()) != NULL) {
		printf("%d : <%s, %d, %d %s>\n", nos, tk->lexeme, tk->row, tk->col, tokenNames[tk->type]);
		nos++;
    }
    fclose(fdi);

	printf("Symbol Table\n");
	int i, j;
	for (i = 0; i <= table_idx; i++) {
		printf("%d\t%s\t%c\t%s\t%d\t%d\t%s\t", i, table[i].name, table[i].scope, symbolDataTypesStr[table[i].type], table[i].size, table[i].no_args, table[i].ret_type != -1? symbolDataTypesStr[table[i].ret_type] : "");

		for (j = 0; j < table[i].no_args; j++)
			printf("%s\t", table[i].args[j]->name);
		printf("\n");
	}


    return 0;
}
