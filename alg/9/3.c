#include "chash.h"

int main(void)
{
    int m, n;
    int i;
    CHT *tab;
    int t, c, j;
    FILE *f;

    for(m = 10; m < 50; m+=10) {
	f = fopen("test", "r");
	for (j = 0; j < 5; j++) {
	    tab = init(m);

		n = (j + 1) * 50;

            for (i = 0; i < n; i++) {
		fscanf(f, "%d", &t);
		insert(tab, t);
	    }

	    c = 0;
	    int r = search(tab, 3721, &c);
	    printf("%d %d %d %d: %d\n", m, n, 3721, r, c);
	    c = 0;
	    r = search(tab, 5, &c);
	    printf("%d %d %d %d: %d\n", m, n, 5, r, c);
	    free(tab);
	}
	fclose(f);
    }

    return 0;
}

