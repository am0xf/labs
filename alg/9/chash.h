#include <stdlib.h>
#include <stdio.h>

typedef struct cht {
    int *ar;
    int mod;
} CHT;

CHT* init(int mod)
{
    CHT *tab = (CHT *) malloc(sizeof(CHT));
    tab->mod = mod;
    tab->ar = (int *) malloc(sizeof(int) * mod);
    int i;
    for(i = 0; i < mod; i++)
	tab->ar[i] = -1;

    return tab;
}

void insert(CHT *tab, int n)
{
    int m = n % tab->mod;

    if (tab->ar[m] == -1)
	tab->ar[m] = n;
    else {
	int i = m + 1;
	while (i != m) {
	    if (tab->ar[i] == -1) {
		tab->ar[i] = n;
		break;
	    }
	    i = (i + 1) % tab->mod;
	}
    }
}

int search(CHT *tab, int n, int *c)
{
    int m = n % tab->mod;

    (*c)++;
    if (tab->ar[m] != n) {
		int i = m + 1;
		while (i != m) {
		    (*c)++;
		    if (tab->ar[i] == n) 
				return n;
		    i = (i + 1) % tab->mod;
		}
		return -1;
    }

    return tab->ar[m];
}

void show(CHT *tab)
{
    int i;
    for(i = 0; i < tab->mod; i++) {
	printf("%d : %d\n", i, tab->ar[i]);
    }
}

