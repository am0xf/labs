#include <stdlib.h>
#include <stdio.h>

typedef struct node {
    int v;
    struct node *next;
} Node;

typedef struct oht {
    Node **ar;
    int mod;
} OHT;

OHT* init(int mod)
{
    OHT *tab = (OHT *) malloc(sizeof(OHT));
    tab->mod = mod;
    tab->ar = (Node **) malloc(sizeof(Node*) * mod);
    int i;
    for(i = 0; i < mod; i++)
	tab->ar[i] = NULL;

    return tab;
}

void insert(OHT *tab, int n)
{
    int m = n % tab->mod;

    Node *t = tab->ar[m];

    while (t && t->next) {
	t = t->next;
    }

    if (t) {
        t->next = (Node *) malloc(sizeof(Node));
	t = t->next;
        t->v = n;
        t->next = NULL;
    }
    else {
        tab->ar[m] = (Node *) malloc(sizeof(Node));
        tab->ar[m]->v = n;
        tab->ar[m]->next = NULL;
    }
}

int search(OHT *tab, int n, int *c)
{
    int m = n % tab->mod;

    Node *t = tab->ar[m];

    while (t && t->v != n) {
	(*c)++;
	t = t->next;
    }

    return t ? t->v : -1;
}

void show(OHT *tab)
{
    int i;
    Node *t;
    for(i = 0; i < tab->mod; i++) {
	printf("%d : ", i);

	t = tab->ar[i];
	while (t) {
	    printf("%d ", t->v);
	    t = t->next;
	}
	printf("\n");
    }
}

