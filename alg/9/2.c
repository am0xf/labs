#include "hash.h"

int main(void)
{
    int m, n;
    int i;
    OHT *tab;
    int t, c, j;
    FILE *f;

    for(m = 10; m < 50; m+=10) {
	for (j = 0; j < 5; j++) {
	    tab = init(m);
		n = (j + 1) * 50;

		f = fopen("test", "r");
            for (i = 0; i < n; i++) {
		fscanf(f, "%d", &t);
		insert(tab, t);
	    }

		fclose(f);
	    c = 0;
	    search(tab, t, &c);
	    printf("%d %d %d : %d\n", m, n, t, c);
	    c = 0;
	    search(tab, 5, &c);
	    printf("%d %d %d : %d\n", m, n, 5, c);
	    free(tab);
	}
    }

    return 0;
}

