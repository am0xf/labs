#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int* shifttable(char *s, int len)
{
	int *table = (int *) malloc(sizeof(int) * 26);

	int i;
	for (i = 0; i < 26; i++)
		table[i] = len;

	for (i = 0; i < len - 1; i++)
		table[s[i]-96] = len - 1 - i;

	return table;
}
int c;

int match(char *s, char *p, int ls, int lp)
{
	int *table = shifttable(p, lp);

	int i = lp - 1;
	int k;

	while (i < ls) {
		k = 0;

		c++;
		while (k < lp && p[lp - 1 - k] == s[i - k]) {
			k++;
			c++;
		}

		if (k == lp)
			return i - lp + 1;
		else
			i += table[s[i]-96];
	}

	return -1;
}

int main(void)
{
	char *s = (char *) malloc(80);
	char *p = (char *) malloc(80);

	fgets(s, 80, stdin);
	fgets(p, 80, stdin);

	s[strlen(s)-1] = '\0';
	p[strlen(p)-1] = '\0';

	c = 0;
	printf("%d\n", match(s, p, strlen(s), strlen(p)));
	printf("%d\n", c);

	return 0;
}

