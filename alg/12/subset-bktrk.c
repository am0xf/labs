#include <stdio.h>
#include <stdlib.h>

void bktrk(int *set, int n, int m, int *subset, int s)
{ 
    if (n == 0) {
	if (s == 0) {
	    int i;
	    for (i = 0; i < m; i++)
		if (subset[i])
		    printf("%d ", set[i]);
	    printf("\n");
	}
	return;
    }

    bktrk(set, n-1, m, subset, s);
    
    if (set[n-1] <= s) {
	subset[n-1] = 1;
	bktrk(set, n-1, m, subset, s - set[n-1]);
	subset[n-1] = 0;
    }
}

int main(void)
{
    int n;
    scanf("%d", &n);

    int *set = (int *) malloc(sizeof(int) * n);
    int *subset = (int *) malloc(sizeof(int) * n);

    int i;
    for (i = 0; i < n; i++)
	scanf("%d", &set[i]);
	
    int s;
    scanf("%d", &s);

    bktrk(set, n, n, subset, s);

    return 0;
}

