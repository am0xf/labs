#include <stdio.h>
#include <stdlib.h>

void sort(int *a, int *b, int n)
{
    int i, j, t;
    for (i = 0; i < n; i++)
	for (j = 0; j < n - i - 1; j++)
	    if (a[j+1] / b[j+1] < a[j] / b[j]) {
		t = a[j+1];
		a[j+1] = a[j];
		a[j] = t;
		t = b[j+1];
		b[j+1] = b[j];
		b[j] = t;
	    }
}

int soln;

void bktrk(int *w, int *v, int n, int W, int cw, int cv)
{ 
	if (cw > W)
		return;

    if (n == 0) {
		if (cv > soln)
			soln = cv;
		return;
	}
		
	int ub = cv + (W-cw) * (v[n-1] / w[n-1]);

	if (ub < soln)
		return;

    bktrk(w, v, n-1, W, cw, cv);
    
    bktrk(w, v, n-1, W, cw + w[n-1], cv + v[n-1]);
}

int main(void)
{
    int n;
    scanf("%d", &n);

    int *w = (int *) malloc(sizeof(int) * n);
    int *v = (int *) malloc(sizeof(int) * n);

    int i;
    for (i = 0; i < n; i++)
	scanf("%d %d", &w[i], &v[i]);
	
    int W;
    scanf("%d", &W);

    sort(v, w, n);
	soln = 0;
    bktrk(w, v, n, W, 0, 0);

	printf("Value : %d\n", soln);

    return 0;
}

