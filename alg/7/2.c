#include "avl.h"

int main(void)
{
    FILE *f = fopen("test_input", "r");

    int n;
    fscanf(f, "%d", &n);

    Tree *tr = (Tree *) malloc(sizeof(Tree));

    n /= 5;
    int i, t;
    for(i = 0; i < n; i++) {
	fscanf(f, "%d", &t);

	search_ins(tr, t);
	bf_all_nodes(tr->root);
	printf("\n");
    }

    fclose(f);

    bf_all_nodes(tr->root);

    return 0;
}

