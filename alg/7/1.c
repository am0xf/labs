#include "bst.h"

int max(int a, int b)
{
    return a > b ? a : b;
}

int height(Node *rt)
{
    if (rt)
	return 1 + max(height(rt->left), height(rt->right));
    return 0;
}

int balance_factor(Node *rt)
{
    return height(rt->left) - height(rt->right);
}

void bf_all_nodes(Node *rt)
{
    if (!rt)
	return;

    printf("%d %d\n", rt->key, balance_factor(rt));

    bf_all_nodes(rt->left);
    bf_all_nodes(rt->right);
}

int main(void)
{
    FILE *f = fopen("test_input", "r");

    int n;
    fscanf(f, "%d", &n);

    Tree *tr = (Tree *) malloc(sizeof(Tree));

    int i, t;
    for(i = 0; i < n; i++) {
	fscanf(f, "%d", &t);
	search_ins(tr, t);
    }

    fclose(f);

    bf_all_nodes(tr->root);

    return 0;
}

