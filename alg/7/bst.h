#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int key;
    struct node *left, *right;
} Node;

typedef struct {
    Node *root;
} Tree;

int max(int a, int b)
{
    return a > b ? a : b;
}

int height(Node *rt)
{
    if (rt)
	return 1 + max(height(rt->left), height(rt->right));
    return 0;
}

int balance_factor(Node *rt)
{
    return height(rt->left) - height(rt->right);
}

void bf_all_nodes(Node *rt)
{
    if (!rt)
	return;

    printf("%d %d\n", rt->key, balance_factor(rt));

    bf_all_nodes(rt->left);
    bf_all_nodes(rt->right);
}

void search_ins(Tree *tr, int val)
{
    Node **temp = &tr->root;

    while (*temp) {
	if ((*temp)->key > val)
	    temp = &(*temp)->left;
	else if ((*temp)->key < val)
	    temp = &(*temp)->right;
	else {
	    printf("%d found\n", val);
	    return;
	}
    }

    *temp = (Node *) malloc(sizeof(Node));
    (*temp)->key = val;
    printf("%d inserted\n", val);
}

void preorder_real(Node *rt)
{
    if (!rt)
	return;

    printf("%d\n", rt->key);
    preorder_real(rt->left);
    preorder_real(rt->right);
}

void inorder_real(Node *rt)
{
    if (!rt)
	return;

    inorder_real(rt->left);
    printf("%d\n", rt->key);
    inorder_real(rt->right);
}

void postorder_real(Node *rt)
{
    if (!rt)
	return;

    postorder_real(rt->left);
    postorder_real(rt->right);
    printf("%d\n", rt->key);
}

void preorder(Tree *tr)
{
    preorder_real(tr->root);
}

void inorder(Tree *tr)
{
    inorder_real(tr->root);
}

void postorder(Tree *tr)
{
    postorder_real(tr->root);
}

