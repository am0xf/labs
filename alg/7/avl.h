#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int key;
    int bf;
    struct node *left, *right;
} Node;

typedef struct {
    Node *root;
} Tree;

int max(int a, int b)
{
    return a > b ? a : b;
}

int height(Node *rt)
{
    if (rt)
	return 1 + max(height(rt->left), height(rt->right));
    return 0;
}

int balance_factor(Node *rt)
{
    return height(rt->left) - height(rt->right);
}

void bf_all_nodes(Node *rt)
{
    if (!rt)
	return;

    printf("%d %d\n", rt->key, rt->bf);

    bf_all_nodes(rt->left);
    bf_all_nodes(rt->right);
}

void left_rot(Tree *tr, Node *nd)
{
    Node **temp = &tr->root;

    while ((*temp)->key != nd->key) {
	if ((*temp)->key > nd->key) {
	    temp = &(*temp)->left;
	}
	else {
	    temp = &(*temp)->right;
	}
    }

    *temp = nd->right;
    nd->right = (*temp)->left;
    (*temp)->left = nd;

    (*temp)->bf = balance_factor(*temp);
    nd->bf = balance_factor(nd);
}

void right_rot(Tree *tr, Node *nd)
{
    Node **temp = &tr->root;

    while ((*temp)->key != nd->key) {
	if ((*temp)->key > nd->key) {
	    temp = &(*temp)->left;
	}
	else {
	    temp = &(*temp)->right;
	}
    }

    *temp = nd->left;
    nd->left = (*temp)->right;
    (*temp)->right = nd;

    (*temp)->bf = balance_factor(*temp);
    nd->bf = balance_factor(nd);
}

void right_left_rot(Tree *tr, Node *nd)
{
    right_rot(tr, nd->right);

    left_rot(tr, nd);
}

void left_right_rot(Tree *tr, Node *nd)
{
    left_rot(tr, nd->left);

    right_rot(tr, nd);
}

void search_ins(Tree *tr, int val)
{
    Node **temp = &tr->root;

    while (*temp) {
	if ((*temp)->key > val) {
	    temp = &(*temp)->left;
	}
	else if ((*temp)->key < val) {
	    temp = &(*temp)->right;
	}
	else {
	    printf("%d found\n", val);
	    return;
	}
    }

    *temp = (Node *) malloc(sizeof(Node));
    (*temp)->key = val;
    (*temp)->bf = 0;
    printf("%d inserted\n", val);

    Node *t = tr->root;
    Node *unbalanced = NULL;

    int tbf;
    while (t && t->key != val) {
	tbf = balance_factor(t);
	if (tbf < -1 || tbf > 1)
	    unbalanced = t;
	
	if (t->key > val)
	    t = t->left;
	else
	    t = t->right;
    }

    if (!unbalanced)
	return;

    if (val < unbalanced->key) { // left-left or left-right
	if (val < unbalanced->left->key) { // left-left
	    right_rot(tr, unbalanced);
	}
	else { // left-right
	    left_right_rot(tr, unbalanced);
	}
    }
    else { // right-right or right-left
	if (val < unbalanced->right->key) { // right-left
	    right_left_rot(tr, unbalanced);
	}
	else { // right-right
	    left_rot(tr, unbalanced);
	}
    }

    t = tr->root;
    
    while (t) {
	t->bf = balance_factor(t);

	if (t->key > val)
	    t = t->left;
	else
	    t = t->right;
    }
}

void preorder_real(Node *rt)
{
    if (!rt)
	return;

    printf("%d\n", rt->key);
    preorder_real(rt->left);
    preorder_real(rt->right);
}

void inorder_real(Node *rt)
{
    if (!rt)
	return;

    inorder_real(rt->left);
    printf("%d\n", rt->key);
    inorder_real(rt->right);
}

void postorder_real(Node *rt)
{
    if (!rt)
	return;

    postorder_real(rt->left);
    postorder_real(rt->right);
    printf("%d\n", rt->key);
}

void preorder(Tree *tr)
{
    preorder_real(tr->root);
}

void inorder(Tree *tr)
{
    inorder_real(tr->root);
}

void postorder(Tree *tr)
{
    postorder_real(tr->root);
}

