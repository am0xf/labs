#include <stdio.h>
#include <stdlib.h>

int c = 0;
int fact(int n)
{
    if (n == 1)
	return 1;
    c++;
    return n * fact(n-1);
}

int strong(int n)
{
    int b = n, s = 0;

    while (n) {
	s += fact(n % 10);
	n /= 10;
    }

    if (s == b)
	return 1;
    return 0;
}

int main(void)
{
    int n;
    FILE *f = fopen("a4i", "r");
    fscanf(f, "%d", &n);

    int *ar = (int *) malloc(sizeof(int) * n);

    int i = n;
    while (i--)
	fscanf(f, "%d", ar + i);

    while (++i < n)
	printf("%d %d\n", ar[i], strong(ar[i]));

    printf("%d\n", c);
    return 0;
}

