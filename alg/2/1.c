#include <stdio.h>

int main(void)
{
    int a, b, c, m;
    FILE *f = fopen("input", "r");

    while (!feof(f)) {
	fscanf(f, "%d %d", &a, &b);

	m = 0;
	c = (a < b)? a : b;
	c++;

	while (--c && a % c != 0 || b % c != 0) m++;

	printf("%d %d %d %d %d\n", a, b, a + b, c, m+1);
    }

    fclose(f);

    return 0;
}

