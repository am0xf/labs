#include <stdio.h>

int toh(int n)
{
    if (n == 1)
	return 2;

    return toh(n-1) + toh(n-1);
}

int main(void)
{
    int n;
    FILE *f = fopen("a2i", "r");
    fscanf(f, "%d", &n);
    fclose(f);

    printf("%d\n", toh(n) - 1);

    return 0;
}

