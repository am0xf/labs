#include <stdio.h>

int main(void)
{
    int a, b, c, m, i, g;
    FILE *f = fopen("input", "r");

    while (!feof(f)) {
	fscanf(f, "%d %d", &a, &b);

	m = 0;
	c = (a < b)? a : b;
	int d = a, e = b;

	i = 2;
	g = 1;
	while (i <= c) {
	    m++;
	    while (!(d % i) && !(e % i)) {
		m++;
		g *= i;
		d /= i;
		e /= i;
		c = (d < e)? d : e;
	    }
	    i++;
	}

	printf("%d %d %d %d %d\n", a, b, a + b, g, m);
    }

    fclose(f);

    return 0;
}

