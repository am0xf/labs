#include <stdio.h>

int main(void)
{
    int n;

    FILE *f = fopen("a1i", "r");
    fscanf(f, "%d", &n);
    fclose(f);

    int i = 2;

    while (i * i <= n) i++;

    if (i * i > n)
	i--;

    printf("%d\n", i);

    return 0;
}

