#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int **mat;
    int nv, ne;
} AdjMat;

void initAdjMat(AdjMat *graph, int nv)
{
    graph->nv = nv;
    graph->ne = 0;
    graph->mat = (int **) malloc(sizeof(int *) * nv);

    int i;
    for(i = 0; i < nv; i++)
	graph->mat[i] = (int *) calloc(sizeof(int), nv);

}

void addEdgeAdjMat(AdjMat *graph, int v1, int v2)
{
    graph->ne++;
    graph->mat[v1][v2] = 1;
}

void showAdjMat(AdjMat *graph)
{
    int i, j;
    for(i = 0; i < graph->nv; i++) {
	for(j = 0; j < graph->nv; j++)
	    printf("%d ", graph->mat[i][j]);

	printf("\n");
    }
}

void topo_sort_src_rem(AdjMat *graph)
{
    int **m = graph->mat;
    int nv = graph->nv;

    printf("Topo\n");
    int i, j, k;
    for(i = 0; i < nv; i++)
    {
	// find a src
	for(j = 0; j < nv; j++) {
	    for(k = 0; k < nv; k++)
		if (m[k][j] || m[k][j] == -1)
		    break;

	    if (k == nv) // no incoming edge on vertex j
		break;
	}

	if (j == nv) // no src left
	    break;

	// remove src
	for(k = 0; k < nv; k++) {
	    m[j][k] = 0;
	    m[k][j] = -1;
	}

	printf("%d ", j + 1);
    }

    printf("\n");
    if (i < nv)
	printf("No topo sort\n");

}

int main(void)
{
    int nv = 4;
    
    AdjMat *g = (AdjMat *) malloc(sizeof(AdjMat));

    initAdjMat(g, nv);

    addEdgeAdjMat(g, 1 - 1, 2 - 1);
    addEdgeAdjMat(g, 2 - 1, 3 - 1);
    addEdgeAdjMat(g, 2 - 1, 4 - 1);

    showAdjMat(g);

    topo_sort_src_rem(g);

    return 0;
}

