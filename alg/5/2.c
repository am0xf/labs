#include <stdio.h>
#include <stdlib.h>

#define max(a, b) ((a) > (b) ? (a) : (b))

typedef struct node {
    int key;
    struct node *left, *right;
} Node;

typedef struct {
    Node *root;
} Tree;

void search_ins(Tree *tr, int val)
{
    Node **temp = &tr->root;

    while (*temp) {
	if ((*temp)->key > val)
	    temp = &(*temp)->left;
	else if ((*temp)->key < val)
	    temp = &(*temp)->right;
	else {
	    return;
	}
    }

    *temp = (Node *) malloc(sizeof(Node));
    (*temp)->key = val;
}


int height(Node *root)
{
    if(!root)
	return 0;

    int lh = height(root->left);
    int rh = height(root->right);
    return 1 + max(lh, rh);
}

int real_diam(Node *root, int *cur_max_diam)
{
    if(!root)
	return 0;

    int cmd = 1 + height(root->left) + height(root->right);

    if(cmd > *cur_max_diam)
	*cur_max_diam = cmd;

    real_diam(root->left, cur_max_diam);
    real_diam(root->right, cur_max_diam);

    return *cur_max_diam;
}

int diameter(Tree *tr)
{
    int diam = -1;

    diam = real_diam(tr->root, &diam);

    return diam;
}

int main(void)
{
    int ar[] = {12,432,34,465,75,1324,436,7,897,234};

    Tree *tr = (Tree *) malloc(sizeof(Tree));

    int i;
    for(i = 0; i < 11; i++)
	search_ins(tr, ar[i]);

    printf("Diam : %d\n", diameter(tr));

    return 0;
}

