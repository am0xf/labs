#include "graph.h"

int main(void)
{
    AdjMat g;
    int nv;
    scanf("%d", &nv);
    initAdjMat(&g, nv);

    int ne;
    scanf("%d", &ne);

    int i, j, v1, v2, c;

    for (i = 0; i < nv; i++)
	for (j = 0; j < nv; j++)
	    g.mat[i][j] = i == j ? 0 : INT_MAX;

    for (i = 0; i < ne; i++) {
	scanf("%d %d %d", &v1, &v2, &c);
	addEdgeCostAdjMat(&g, v1, v2, c);
    }

    showAdjMat(&g);

    allpairshortestpath(&g);

    showAdjMat(&g);

    return 0;
}

