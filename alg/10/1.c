#include "graph.h"

int main(void)
{
    AdjMat g;
    int nv;
    scanf("%d", &nv);
    initAdjMat(&g, nv);

    int ne;
    scanf("%d", &ne);

    int i, v1, v2;
    for (i = 0; i < ne; i++) {
	scanf("%d %d", &v1, &v2);
	addEdgeAdjMat(&g, v1, v2);
    }

    showAdjMat(&g);

    transitiveclosure(&g);

    showAdjMat(&g);

    return 0;
}

