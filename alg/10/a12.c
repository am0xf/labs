#include <stdio.h>
#include <stdlib.h>

int max(int a, int b)
{
    return a > b ? a : b;
}

int **m, *v, *w;

int knapsack(int n, int W)
{
    if (n == 0 || W == 0)
	return m[n][W];

    if (m[n][W] < 0) {
	if (W < w[n])
	    m[n][W] = knapsack(n-1, W);
	else
	    m[n][W] = max(knapsack(n-1, W), v[n] + knapsack(n-1, W - w[n]));
    }

    return m[n][W];
} 

void composition(int n, int W)
{
    if (n == 0 || W == 0)
	return;

    if (m[n-1][W] == m[n][W])
	composition(n-1, W);
    else {
	printf("%d %d %d\n", n, v[n], w[n]);
	composition(n-1, W - w[n]);
    }
}

int main(void)
{
    int n, W;
    scanf("%d %d", &n, &W);

    v = (int *) malloc(sizeof(int) * (n+1));
    w = (int *) malloc(sizeof(int) * (n+1));

    int i;
    for (i = 1; i <= n; i++)
	scanf("%d %d", &w[i], &v[i]);

    int j;
    m = (int **) malloc(sizeof(int *) * (n+1));
    for (i = 0; i <= n; i++) {
	m[i] = (int *) malloc(sizeof(int) * (W+1));
	for (j = 0; j <= W; j++)
		m[i][j] = (i == 0 || j == 0) ? 0 : -1;
    }

    printf("%d\n", knapsack(n, W));
    composition(n, W);

    return 0;
}

