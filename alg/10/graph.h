#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

typedef struct {
    int **mat;
    int nv, ne;
} AdjMat;

typedef struct List {
    int v;
    struct List *next;
} List;

typedef struct Node {
    int v;
    int visited;
    struct Node *next;
    struct List *adjlist;
} Node;

typedef struct AdjList {
    int nv, ne;
    Node *lv;
} AdjList;

AdjList* initAdjList(void)
{
    AdjList *graph = (AdjList *) malloc(sizeof(AdjList));
    graph->nv = graph->ne = 0;
    graph->lv = NULL;
    return graph;
}

void addEdgeAdjList(AdjList *graph, int v1, int v2)
{
    graph->ne++;
    
    Node **tmp = &graph->lv;

    while(*tmp && (*tmp)->v != v1)
	tmp = &(*tmp)->next;

    if (!*tmp) {
	*tmp = (Node *) malloc(sizeof(Node));
	(*tmp)->v = v1;
	graph->nv++;
	(*tmp)->adjlist = (List *) malloc(sizeof(List));
	(*tmp)->adjlist->v = v2;
    }
    else {
	List **temp = &(*tmp)->adjlist;

	while (*temp && (*temp)->v != v2)
	    temp = &(*temp)->next;

	if (*temp && (*temp)->v == v2)
	    return;

	*temp = (List *) malloc(sizeof(List));
	(*temp)->v = v2;
    }

    // add the v2 to adjlist
    tmp = &graph->lv;

    while(*tmp && (*tmp)->v != v2)
	tmp = &(*tmp)->next;

    if(*tmp && (*tmp)->v == v2)
	return;

    *tmp = (Node *) malloc(sizeof(Node));
    (*tmp)->v = v2;
    graph->nv++;
    (*tmp)->adjlist = NULL;
}

void showAdjList(AdjList *graph)
{
    Node *temp = graph->lv;
    List *tmp;

    while(temp) {
	tmp = temp->adjlist;
	printf("%d: ", temp->v);

	while(tmp) {
	    printf("%d, ", tmp->v);
	    tmp = tmp->next;
	}

	printf("\n");
	temp = temp->next;
    }
}

void initAdjMat(AdjMat *graph, int nv)
{
    graph->nv = nv;
    graph->ne = 0;
    graph->mat = (int **) malloc(sizeof(int *) * nv);

    int i;
    for(i = 0; i < nv; i++)
	graph->mat[i] = (int *) calloc(sizeof(int), nv);

}

void addUEdgeAdjMat(AdjMat *graph, int v1, int v2)
{
    graph->ne += 2;
    graph->mat[v1][v2] = 1;
    graph->mat[v2][v1] = 1;
}

void addEdgeAdjMat(AdjMat *graph, int v1, int v2)
{
    graph->ne++;
    graph->mat[v1][v2] = 1;
}

void addEdgeCostAdjMat(AdjMat *graph, int v1, int v2, int c)
{
    graph->ne++;
    graph->mat[v1][v2] = c;
}

void showAdjMat(AdjMat *graph)
{
    int i, j;
    for(i = 0; i < graph->nv; i++) {
	for(j = 0; j < graph->nv; j++)
	    printf("%d ", graph->mat[i][j] == INT_MAX ? -1 : graph->mat[i][j]);

	printf("\n");
    }
    printf("\n");
}

void transitiveclosure(AdjMat *g)
{
    // Warshall

    int i, j, k;

    for (k = 0; k < g->nv; k++)
	for (i = 0; i < g->nv; i++)
	    for (j = 0; j < g->nv; j++)
		g->mat[i][j] = g->mat[i][j] || (g->mat[i][k] && g->mat[k][j]);
}

int minmod(int a, int b, int c)
{
    if (b == INT_MAX || c == INT_MAX)
	return a;
    return a < (b+c) ? a : (b+c);
}

void allpairshortestpath(AdjMat *g)
{
    // Floyd

    int i, j, k;

    for (k = 0; k < g->nv; k++)
	for (i = 0; i < g->nv; i++)
	    for (j = 0; j < g->nv; j++)
		g->mat[i][j] = minmod(g->mat[i][j], g->mat[i][k], g->mat[k][j]);
}


void dfs(AdjList *graph, Node *n)
{
    if(!n || n->visited)
	return;

    printf("Visited : %d\n", n->v);
    n->visited = 1;

    int next_v = 0;

    struct List *tmp = n->adjlist;
    struct Node *temp = graph->lv;

    // cycle through unvisited neighbors
    while(tmp){
	temp = graph->lv;

	// search for the neighbor in AdjList 
	while(temp) {
	    if (temp->v == tmp->v)
		break;
	    temp = temp->next;
	}

	printf("Pushed : %d\n", temp->v);
	dfs(graph, temp);
	printf("Popped : %d\n", temp->v);
	
	tmp = tmp->next;
    }
}

void DFS(AdjList *g)
{    
    struct Node *temp = g->lv;

    // cycle through unvisited neighbors
    while(temp){
	if(!temp->visited) {
	    printf("Pushed : %d\n", temp->v);
	    dfs(g, temp);
	    printf("Popped : %d\n", temp->v);
	}
	temp = temp->next;
    }
}

void bfs(AdjList *graph, Node *n)
{
    if(!n)
	return;

    n->visited = 1;

    int next_v = 0;

    struct List *tmp = n->adjlist;
    struct Node *temp = graph->lv;

    // cycle through unvisited neighbors and mark
    while(tmp){
	temp = graph->lv;

	// search for the neighbor in AdjList 
	while(temp) {
	    if (temp->v == tmp->v)
		break;
	    temp = temp->next;
	}

	// if not visited then mark
	if(!temp->visited) {
	    printf("Visited : %d\n", temp->v);
	    temp->visited = 1;
	}
	
	tmp = tmp->next;
    }

    tmp = n->adjlist;

    // cycle through unvisited neighbors and visited 
    while(tmp){
	temp = graph->lv;

	// search for the neighbor in AdjList 
	while(temp) {
	    if (temp->v == tmp->v)
		break;
	    temp = temp->next;
	}

	// if not visited then mark
	
	printf("Pushed : %d\n", temp->v) ;
        bfs(graph, temp);
        printf("Popped : %d\n", temp->v); 

	tmp = tmp->next;
    }

}

void BFS(AdjList *g)
{    
    struct Node *temp = g->lv;

    // cycle through unvisited neighbors
    while(temp){
	if(!temp->visited) {
	    bfs(g, temp);
	}
	temp = temp->next;
    }
}

/*void trav_sales(int **m, int c, int start, int cost)
{
    int i;
    int flag = 0;
    int tc = 0;

    for(i = 0; i < c; i++) {
	if (m[start][i] == 0 || visited[i])
	    continue;

	visited[i] = 1;
	flag = 1;

	tc = m[start][i];

	// If backtracking uncomment if condition
	//if (min_cost == 0 || cost + tc < min_cost)
	    f(m, c, i, cost+tc);

	visited[i] = 0;
    }

    // if all visited

    if (flag == 0) {
	cost += m[start][start_org];
	if (cost < min_cost || min_cost == 0)
	    min_cost = cost;
    }
}*/

void topo_sort_src_rem(AdjMat *graph)
{
    int **m = graph->mat;
    int nv = graph->nv;

    printf("Topo\n");
    int i, j, k;
    for(i = 0; i < nv; i++)
    {
	// find a src
	for(j = 0; j < nv; j++) {
	    for(k = 0; k < nv; k++)
		if (m[k][j] || m[k][j] == -1) // -1 means already checked
		    break;

	    if (k == nv) // no incoming edge on vertex j
		break;
	}

	if (j == nv) // no src left
	    break;

	// remove src
	for(k = 0; k < nv; k++) {
	    m[j][k] = 0;
	    m[k][j] = -1; // checked
	}

	printf("%d ", j + 1);
    }

    printf("\n");
    if (i < nv)
	printf("No topo sort\n");

}


