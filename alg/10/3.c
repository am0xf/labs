#include <stdio.h>
#include <stdlib.h>

int max(int a, int b)
{
    return a > b ? a : b;
}

int knapsack(int *v, int *w, int n, int W)
{
    int **m = (int **) malloc(sizeof(int*) * (n+1));

    int i;

    for (i = 0; i <= n; i++)
	m[i] = (int *) calloc(sizeof(int), W + 1);

    int j;

    for (i = 1; i <= n; i++)
	for (j = 1; j <= W; j++)
	    m[i][j] = (j - w[i] >= 0) ? max(m[i-1][j], v[i] + m[i-1][j-w[i]]) : m[i-1][j];

    for (i = 0; i <= n; i++) {
	for (j = 0; j <= W; j++)
	    printf("%d ", m[i][j]);
	printf("\n");
    }

    return m[n][W];
}

int main(void)
{
    int n, W;
    scanf("%d %d", &n, &W);

    int *v = (int *) malloc(sizeof(int) * (n+1));
    int *w = (int *) malloc(sizeof(int) * (n+1));

    int i;
    for (i = 1; i <= n; i++)
	scanf("%d %d", &w[i], &v[i]);

    printf("%d\n", knapsack(v, w, n, W));

    return 0;
}

