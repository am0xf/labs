#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int key;
    struct node *left, *right;
} Node;

typedef struct {
    Node *root;
} Tree;

void search_ins(Tree *tr, int val)
{
    Node **temp = &tr->root;

    while (*temp) {
	if ((*temp)->key > val)
	    temp = &(*temp)->left;
	else if ((*temp)->key < val)
	    temp = &(*temp)->right;
	else {
	    printf("%d found\n", val);
	    return;
	}
    }

    *temp = (Node *) malloc(sizeof(Node));
    (*temp)->key = val;
    printf("%d inserted\n", val);
}

void preorder_real(Node *rt)
{
    if (!rt)
	return;

    printf("%d\n", rt->key);
    preorder_real(rt->left);
    preorder_real(rt->right);
}

void inorder_real(Node *rt)
{
    if (!rt)
	return;

    inorder_real(rt->left);
    printf("%d\n", rt->key);
    inorder_real(rt->right);
}

void postorder_real(Node *rt)
{
    if (!rt)
	return;

    postorder_real(rt->left);
    postorder_real(rt->right);
    printf("%d\n", rt->key);
}

void preorder(Tree *tr)
{
    preorder_real(tr->root);
}

void inorder(Tree *tr)
{
    inorder_real(tr->root);
}

void postorder(Tree *tr)
{
    postorder_real(tr->root);
}

