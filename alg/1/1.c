#include "bst.h"

int main(void)
{
    int ar[] = {12,432,34,465,12, 75,1324,436,7,897,234};

    Tree *tr = (Tree *) malloc(sizeof(Tree));

    int i;
    for(i = 0; i < 11; i++)
	search_ins(tr, ar[i]);
    
    printf("\nPreorder\n");
    preorder(tr);

    printf("\nInorder\n");
    inorder(tr);

    printf("\nPostorder\n");
    postorder(tr);

    return 0;
}

