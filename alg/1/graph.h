#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int **mat;
    int nv, ne;
} AdjMat;

typedef struct List {
    int v;
    struct List *next;
} List;

typedef struct Node {
    int v;
    struct Node *next;
    struct List *adjlist;
} Node;

typedef struct AdjList {
    int nv, ne;
    Node *lv;
} AdjList;

void initAdjList(AdjList *graph)
{
    graph->nv = graph->ne = 0;
    graph->lv = NULL;
}

void addEdgeAdjList(AdjList *graph, int v1, int v2)
{
    graph->ne++;
    
    Node **tmp = &graph->lv;

    while(*tmp && (*tmp)->v != v1)
	tmp = &(*tmp)->next;

    if (!*tmp) {
	*tmp = (Node *) malloc(sizeof(Node));
	(*tmp)->v = v1;
	graph->nv++;
	(*tmp)->adjlist = (List *) malloc(sizeof(List));
	(*tmp)->adjlist->v = v2;
    }
    else {
	List **temp = &(*tmp)->adjlist;

	while (*temp && (*temp)->v != v2)
	    temp = &(*temp)->next;

	if (*temp && (*temp)->v == v2)
	    return;

	*temp = (List *) malloc(sizeof(List));
	(*temp)->v = v2;
    }
}

void showAdjList(AdjList *graph)
{
    Node *temp = graph->lv;
    List *tmp;

    while(temp) {
	tmp = temp->adjlist;
	printf("%d: ", temp->v);

	while(tmp) {
	    printf("%d, ", tmp->v);
	    tmp = tmp->next;
	}

	printf("\n");
	temp = temp->next;
    }
}

void initAdjMat(AdjMat *graph, int nv)
{
    graph->nv = nv;
    graph->ne = 0;
    graph->mat = (int **) malloc(sizeof(int *) * nv);

    int i;
    for(i = 0; i < nv; i++)
	graph->mat[i] = (int *) calloc(sizeof(int), nv);

}

void addUEdgeAdjMat(AdjMat *graph, int v1, int v2)
{
    graph->ne += 2;
    graph->mat[v1][v2] = 1;
    graph->mat[v2][v1] = 1;
}

void addEdgeAdjMat(AdjMat *graph, int v1, int v2)
{
    graph->ne++;
    graph->mat[v1][v2] = 1;
}

void showAdjMat(AdjMat *graph)
{
    int i, j;
    for(i = 0; i < graph->nv; i++) {
	for(j = 0; j < graph->nv; j++)
	    printf("%d ", graph->mat[i][j]);

	printf("\n");
    }
}

