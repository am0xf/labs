#include "../graph.h"

int main(void)
{
    int nv = 5;
    
    AdjMat *g = (AdjMat *) malloc(sizeof(AdjMat));

    initAdjMat(g, nv);

    addEdgeAdjMat(g, 1, 2);
    addEdgeAdjMat(g, 1, 3);
    addEdgeAdjMat(g, 4, 2);
    addEdgeAdjMat(g, 1, 4);
    addEdgeAdjMat(g, 0, 2);
    addEdgeAdjMat(g, 0, 1);

    showAdjMat(g);

    AdjList *h = (AdjList *) malloc(sizeof(AdjList));

    initAdjList(h);

    addEdgeAdjList(h, 1, 2);
    addEdgeAdjList(h, 1, 3);
    addEdgeAdjList(h, 4, 2);
    addEdgeAdjList(h, 1, 4);
    addEdgeAdjList(h, 0, 2);
    addEdgeAdjList(h, 0, 1);

    showAdjList(h);

    return 0;
}

