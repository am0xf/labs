#include <stdlib.h>

typedef struct selem {
    void *p;
    struct selem *next;
} SElem;

typedef struct {
    SElem *top;
} Stack;

Stack* initStack(void)
{
    Stack *s = (Stack *) malloc(sizeof(Stack));
    s->top = NULL;

    return s;
}

void push(Stack *s, void *p)
{
    SElem *ne = (SElem *) malloc(sizeof(SElem));
    ne->next = s->top;
    ne->p = p;
    s->top = ne;
}

void* pop(Stack *s)
{
    if (!s->top)
	return NULL;

    SElem *u = s->top;
    void *t = u->p;
    s->top = s->top->next;
    free(u);

    return t;
}

