#include <stdio.h>

#include "graph.h"

int main(void)
{
    AdjList *h = initAdjList();

    printf("Directed\n");
    addEdgeAdjList(h, 1, 2);
    addEdgeAdjList(h, 1, 3);
    addEdgeAdjList(h, 2, 4);
    addEdgeAdjList(h, 4, 1);

    showAdjList(h);

    printf("DFS\n");
    DFS(h);

    h = initAdjList();

    printf("Directed\n");
    addEdgeAdjList(h, 1, 2);
    addEdgeAdjList(h, 1, 3);
    addEdgeAdjList(h, 2, 4);
    addEdgeAdjList(h, 4, 1);

    showAdjList(h);


    printf("BFS\n");
    BFS(h);

    return 0;
}

