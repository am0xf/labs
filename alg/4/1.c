#include <stdio.h>
#include <stdlib.h>

void solve(int **costs, int nw, int nj, int c, int *nc)
{
    if (nw == 0) {
	*nc = (*nc == -1 || c < *nc) ? c : *nc;

	return;
    }

    int *bkp = (int *) malloc(sizeof(int) * nw);

    int i, j;
    for(i = 0; i < nj; i++) {
	if(!costs[0][i])
	    continue;

	// backup costs of job j
	for(j = 1; j < nw; j++) {
	    bkp[j] = costs[j][i];
	    costs[j][i] = 0;
	}

	solve(costs + 1, nw - 1, nj, costs[0][i] + c, nc);

	// restor costs of job j
	for(j = 1; j < nw; j++) {
	    costs[j][i] = bkp[j];
	}

    }

    free(bkp);
}

int main(void)
{
    int **costs, nw, nj, mc = -1;

    FILE *f = fopen("1i", "r");

    fscanf(f, "%d %d", &nw, &nj);

    int i, j;

    costs = (int **) malloc(sizeof(int *) * nw);

    for(i = 0; i < nw; i++) {
	costs[i] = (int *) malloc(sizeof(int) * nj);

	for(j = 0; j < nj; j++)
	    fscanf(f, "%d", *(costs+i)+j);
    }

    fclose(f);

    solve(costs, nw, nj, 0, &mc);

    printf("%d\n", mc);

    return 0;
}

