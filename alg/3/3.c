#include <stdio.h>
#include <stdlib.h>

#include <math.h>

void check(int *ar, int n)
{
    long bitmap = (long) pow(2, n) - 1;
    long t, s1, s2, i;

    while (bitmap != -1) {
	t = bitmap;
	s1 = s2 = 0;

	i = 0;

	while(t) {
	    if (t % 2)
		s1 += ar[i++];
	    else
		s2 += ar[i++];

	    t /= 2;
	}

	while (i < n)
	    s2 += ar[i++];

	if (s1 == s2) { 
	    t = bitmap;
	    i = 0;

	    while(t) {
		printf("%ld ", t % 2);
		t /= 2;
		i++;
	    }

	    while (i < n) {
		printf("0 ");
		i++;
	    }

	    printf("\n");
	}

	bitmap--;
    }
}
	

int main(void)
{
    int n, *ar;

    FILE *f = fopen("3i", "r");

    fscanf(f, "%d", &n);

    ar = (int *) malloc(sizeof(int) * n);

    int i;

    for(i = 0; i < n; i++)
	fscanf(f, "%d", ar + i);

    fclose(f);

    for(i = 0; i < n; i++)
	printf("%d ", ar[i]);
    printf("\n");

    check(ar, n);

    return 0;
}

