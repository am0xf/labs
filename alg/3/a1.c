#include <stdio.h>
#include <stdlib.h>

void multiply(int **mat1, int **mat2, int **res, int r1, int c1, int r2, int c2) 
{ 
    if (c1 != r2)
	return;

    int i, j, k = 0; 
    for (i = 0; i < r1; i++) 
    { 
        for (j = 0; j < c2; j++) 
        { 
            res[i][j] = 0; 

            for (k = 0; k < c1; k++) 
                res[i][j] += mat1[i][k] * mat2[k][j]; 
        } 
    } 
} 

int main(void)
{
    int **a1, **a2, **r, r1, c1, r2, c2;
    int i, j;

    FILE *f = fopen("a1i", "r");

    fscanf(f, "%d", &r1);
    fscanf(f, "%d", &c1);
    a1 = (int **) malloc(sizeof(int *) * r1);
    r = (int **) malloc(sizeof(int *) * r1);
    for(i = 0; i < r1; i++) {
	a1[i] = (int *) malloc(sizeof(int) * c1);

	for(j = 0; j < c1; j++)
	    fscanf(f, "%d", *(a1 + i) + j);
    }

    fscanf(f, "%d", &r2);
    fscanf(f, "%d", &c2);
    a2 = (int **) malloc(sizeof(int *) * r2);
    for(i = 0; i < r2; i++) {
	a2[i] = (int *) malloc(sizeof(int) * c2);
	r[i] = (int *) malloc(sizeof(int) * c2);

	for(j = 0; j < c2; j++)
	    fscanf(f, "%d", *(a2 + i) + j);
    }

    multiply(a1, a2, r, r1, c1, r2, c2);

    for(i = 0; i < r1; i++) {
	for(j = 0; j < c1; j++)
	    printf("%d ", a1[i][j]);
	printf("\n");
    }

    for(i = 0; i < r2; i++) {
	for(j = 0; j < c2; j++)
	    printf("%d ", a2[i][j]);
	printf("\n");
    }

    for(i = 0; i < r1; i++) {
	for(j = 0; j < c2; j++)
	    printf("%d ", r[i][j]);
	printf("\n");
    }

    return 0;
}

