#include <stdio.h>
#include <stdlib.h>

void selsort(int *ar, int n)
{
    if (n == 1)
	return;

    int i, j, t, tmp;

    for(i = 0; i < n; i++) {
	t = i;
	for(j = i + 1; j < n; j++)
	    if (ar[t] > ar[j])
		t = j;
	
	tmp = ar[t];
	ar[t] = ar[i];
	ar[i] = tmp;
    }
}

void bubsort(int *ar, int n)
{
    if (n == 1)
	return;

    int i, j, t;

    for(i = 0; i < n; i++)
	for(j = 0; j < n - i; j++)
	    if (ar[j] > ar[j+1]) {
		t = ar[j];
		ar[j] =  ar[j+1];
		ar[j+1] = t;
	    }	
}


int main(void)
{
    int n, *ar;

    FILE *f = fopen("1i", "r");

    fscanf(f, "%d", &n);

    ar = (int *) malloc(sizeof(int) * n);

    int i;

    for(i = 0; i < n; i++)
	fscanf(f, "%d", ar + i);

    fclose(f);

    bubsort(ar, n);

    for(i = 0; i < n; i++)
	printf("%d ", ar[i]);
    printf("\n");

    return 0;
}

