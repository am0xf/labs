#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#define LEN 80

int search(char *s, char *p)
{
    int ls = strlen(s);
    int lp = strlen(p);

    int i, j;

    for(i = 0; i <= ls - lp; i++) {
	j = 0;
	while (j < lp && s[i+j] == p[j]) 
	    j++;

	if (j == lp)
	    return i + 1;
    }

    return -1;
}

int main(void)
{
    char *s, *p;
    s = (char *) malloc(LEN);
    p = (char *) malloc(LEN);

    FILE *f = fopen("2i", "r");

    fscanf(f, "%s", s);
    fscanf(f, "%s", p);

    fclose(f);

    int i = search(s, p);

    printf("%d\n", i);

    return 0;
}

