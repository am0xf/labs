#include "bst.h"

int non(Node *root)
{
    if (!root)
	return 0;

    return 1 + non(root->left) + non(root->right);
}

int NON(Tree *tr)
{
    return non(tr->root);
}

int main(void)
{
    int ar[] = {12,432,34,465,75,1324,436,7,897,234};

    Tree *tr = (Tree *) malloc(sizeof(Tree));

    int i;
    for(i = 0; i < 10; i++)
	search_ins(tr, ar[i]);

    printf("No. of Nodes : %d\n", NON(tr));

    return 0;
}

