#include <stdlib.h>
#include <stdio.h>

int c;

int *mergesort(int *arr, int size)
{
    int *oarr = (int *) malloc(sizeof(int) * size);

    if (size == 1) {
	oarr[0] = arr[0];
	return oarr;
    }

    int *larr = mergesort(arr, size / 2);
    int *rarr = mergesort(arr + size / 2, size - size / 2);

    int lp = 0, rp = 0, op = 0;

    while (lp < size / 2 && rp < size - size / 2) {
	if (larr[lp] < rarr[rp]) {
	    c++;
	    oarr[op++] = larr[lp++];
	}
	else
	    oarr[op++] = rarr[rp++];
    }

    while (lp < size / 2)
	oarr[op++] = larr[lp++];

    while (rp < size - size / 2)
	oarr[op++] = rarr[rp++];

    free(larr);
    free(rarr);

    return oarr;
}

int main(void)
{
    int *ar, n;
    int tc;
    int i;

    FILE *f = fopen("test_input", "r");

    for(tc = 0; tc < 5; tc++) {
	fscanf(f, "%d", &n);
	ar = (int *) malloc(sizeof(int) * n);

	for(i = 0; i < n; i++)
	    fscanf(f, "%d", &ar[i]);

	c = 0;
	mergesort(ar, n);

	free(ar);

	printf("%d %d\n", n, c);
    }

    fclose(f);

    return 0;
}

