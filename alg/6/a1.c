#include <stdio.h>

int comp(int a, int n)
{
    if (n == 1)
	return a;

    return comp(a, n / 2) * comp(a, n - n / 2);
}

int main(void)
{
    int a, n;
    scanf("%d %d", &a, &n);
    printf("%d ^ %d = %d\n", a, n, comp(a, n));

    return 0;
}

