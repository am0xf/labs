#include <stdio.h>

int comp(int n)
{
    if (n < 3)
	return n;

    return comp(n-1) + comp(n-2);
}

int main(void)
{
    int n;
    scanf("%d", &n);

    printf("2 * %d : %d\n", n, comp(n));

    return 0;
}

