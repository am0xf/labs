#!/usr/bin/env python3

import random

f = open('test_input', 'w')
for test_case in [50, 100, 150, 200, 250]:
	f.write(str(test_case) + '\n')
	for j in range(test_case):
		f.write(str(int(random.random() * 100000)) + '\n')

f.close()

