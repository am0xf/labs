#include <stdio.h>
#include <stdlib.h>

int c;

void quicksort(int *arr, int size)
{
    if (size < 2)
	return;
    // pivot = arr[0]

    int lh = 1, rh = size - 1;
    int t;

    while (rh >= lh) {
	c++;
	while (arr[lh] <= arr[0]) {
	    lh++;
	    c++;
	}

	c++;
	while (arr[rh] > arr[0]) {
	    rh--;
	    c++;
	}

	t = arr[lh];
	arr[lh] = arr[rh];
	arr[rh] = t;

    }

    t = arr[lh];
    arr[lh] = arr[rh];
    arr[rh] = t;

    t = arr[rh];
    arr[rh] = arr[0];
    arr[0] = t;

    quicksort(arr, rh);
    quicksort(arr + lh, size - rh - 1);
}

int main(void)
{
    int *ar, n;
    int tc;
    int i;

    FILE *f = fopen("test_input", "r");

    for(tc = 0; tc < 5; tc++) {
	fscanf(f, "%d", &n);
	ar = (int *) malloc(sizeof(int) * n);

	for(i = 0; i < n; i++)
	    fscanf(f, "%d", &ar[i]);

	c = 0;
	quicksort(ar, n);

	free(ar);

	printf("%d %d\n", n, c);
    }

    fclose(f);

    return 0;
}

