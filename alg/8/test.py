#!/usr/bin/env python3

import random

f = open('test', 'w')

for i in [50, 100, 150, 200, 250]:

    f.write(str(i) + '\n')
    for j in range(i):
        f.write(str(int(random.random() * 100000)) + '\n')

f.close()

