#include "heap.h"

int main(void)
{
    int n = 5;
    int *ar = (int *) malloc(sizeof(int) * (n+1));

    int i;
    for(i = 1; i <= n; i++) {
	ar[i] = i;
    }

    heapBU(ar, n);
    printheap(ar, n);

    n = delsmallest(ar, n);
    printheap(ar, n);

    return 0;
}

