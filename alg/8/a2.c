#include "heap.h"

int main(void)
{
    int *ar = (int *) malloc(sizeof(int) * 6);

    int i;
    for(i = 1; i <= 5; i++) {
	ar[i] = i;
    }

    heapBU(ar, 5);
    printheap(ar, 5);

    delelem(ar, 5, 4);
    printheap(ar, 4);

    return 0;
}

