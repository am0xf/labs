#include "heap.h"

int main(void)
{
    int *ar = (int *) malloc(sizeof(int) * 6);

    int i;
    for(i = 1; i <= 5; i++) {
	ar[i] = i;
    }

    heapBU(ar, 5);
    printheap(ar, 5);
    printf("Heap : %d\n\n", checkheap(ar, 5, 1));
    heapsort(ar, 5);
    printheap(ar, 5);
    printf("Heap : %d\n\n", checkheap(ar, 5, 1));
    return 0;
}

