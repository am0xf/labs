#include <stdlib.h>
#include <stdio.h>

// Assume 1 index

int cost;
void swap(int *a, int *b)
{
    if (a != b) {
        int t = *a;
        *a = *b;
	*b = t;
    }
}

void heapTD(int *ar, int i)
{
    cost++;
    if (i > 1 && ar[i] > ar[i/2]) {
	swap(ar + i/2, ar + i);
	heapTD(ar, i/2);
    }
}

void printheap(int *ar, int n)
{
    int i;
    for(i = 1; i <= n; i++)
	printf("%d\n", ar[i]);
    printf("\n");
}

void heapBU(int *ar, int n)
{
    int i, j, k, v, h = 0;
    for(i = n / 2; i > 0; i--) {
	k = i;
	v = ar[k];

	h = 0;

	while (!h && 2 * k <= n) {
	    j = 2 * k;

	    if (j < n) {
		cost++;
		if (ar[j] < ar[j+1])
		    j = j + 1;
	    }
	    cost++;

	    if (v >= ar[j])
		h = 1;
	    else {
		ar[k] = ar[j];
		k = j;
	    }
	}

	ar[k] = v;
    }
}

void heapify(int *ar, int n, int i)
{
    cost++;
    int l = (2*i <= n && ar[i] < ar[2*i]) ? 2*i : i;
    cost++;
    l = (2*i+1 <= n && ar[l] < ar[2*i+1]) ? 2*i+1 : l;

    swap(ar + i, ar + l);

    l == i ? 0 : heapify(ar, n, l);
}

void heapsort(int *ar, int n)
{
    int t = n;

    cost++;
    while (t > 1) {
	swap(ar + 1, ar + t);
	t--;
	heapify(ar, t, 1);
	cost++;
    }
}

int checkheap(int *ar, int n, int i)
{
    if (i > n / 2)
	return 1;

    return (ar[i] > ar[2*i] && ar[i] > ar[2*i+1]) && checkheap(ar, n , 2*i) && checkheap(ar, n, 2*i+1);
}

int delelem(int *ar, int n, int v)
{
    int i = 1;
    while (ar[i] != v && i <= n)
	i++;

    if (i > n)
	return n;

    swap(ar + i, ar + n);

    heapify(ar, n-1, i);

    return n-1;
}

int delsmallest(int *ar, int n)
{
    int i = (n / 2) * 2 + 1, s = i - 1;

    while (i <= n) {
	s = ar[s] > ar[i] ? i : s;
	i++;
    }

    swap(ar + s, ar + n);

    heapTD(ar, s);

    return n-1;
}        

