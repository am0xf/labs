#include <stdio.h>
#include <stdlib.h>

int **soln;

int max(int a, int b, int c)
{
	if (a > b) {
		if (a > c)
			return a;
		else
			return c;
	}

	else {
		if (b > c)
			return b;
		else
			return c;
	}
}

void solve(int **mat, int n, int m)
{
	if (n == 0 || m == 0)
		return;

	solve(mat, n-1, m-1);
	solve(mat, n, m-1);
	solve(mat, n-1, m);

	soln[n-1][m-1] = mat[n-1][m-1];

	if (n == 1) {
		if (m == 1)
			soln[n-1][m-1] += mat[n-1][m-1];
		else
			soln[n-1][m-1] += mat[n-1][m-2];
	}
	else if (m == 1) {
		if (n == 1)
			soln[n-1][m-1] += mat[n-1][m-1];
		else
			soln[n-1][m-1] += mat[n-2][m-1];
	}
	else {
		soln[n-1][m-1] += max(soln[n-2][m-2], soln[n-2][m-1], soln[n-1][m-2]);
	}
}

void composition(int **mat, int **soln, int n, int m)
{
	if (n == 0 || m == 0)
		return;

	if (mat[n-1][m-1] == 1)
		printf("%d %d\n", n-1, m-1);

	if (n == 1)
		composition(mat, soln, n, m-1);
	else if (m == 1)
		composition(mat, soln, n-1, m);
	else {
		int k = max(soln[n-2][m-2], soln[n-1][m-2], soln[n-2][m-1]);
		if (soln[n-2][m-2] == k)
			composition(mat, soln, n-1, m-1);
		else if (soln[n-1][m-2] == k)
			composition(mat, soln, n, m-1);
		else if (soln[n-2][m-1] == k)
			composition(mat, soln, n-1, m);
	}
}

int main(void)
{
	int n, m;
	scanf("%d %d", &n, &m);

	int **mat = (int **) malloc(sizeof(int *) * n);
	soln = (int **) malloc(sizeof(int *) * n);
	
	int i, j;

	for (i = 0; i < n; i++) {
		mat[i] = (int *) malloc(sizeof(int) * m);
		soln[i] = (int *) malloc(sizeof(int) * m);

		for (j = 0; j < m; j++)
			mat[i][j] = soln[i][j] = 0;

	}

	int k, u, v;
	scanf("%d", &k);

	for (i = 0; i < k; i++) {
		scanf("%d %d", &u, &v);
		mat[u][v] = 1;
	}

	solve(mat, n, m);

	for (i = 0; i < n; i++) {
		for (j = 0; j < m; j++)
			printf("%d ", soln[i][j]);
		printf("\n");

	}

	printf("Total collected : %d\n", soln[n-1][m-1]);
	printf("Composition\n");
	composition(mat, soln, n, m);

	return 0;
}


