#include <stdio.h>
#include <stdlib.h>

int min_cost;
int *visited;
int start_org;

void trav_sales(int **m, int c, int start, int cost)
{
    int i;
    int flag = 0;
    int tc = 0;

    for(i = 0; i < c; i++) {
	if (m[start][i] == 0 || visited[i])
	    continue;

	visited[i] = 1;
	flag = 1;

	tc = m[start][i];

	// If backtracking uncomment if condition
	//if (min_cost == 0 || cost + tc < min_cost)
	    f(m, c, i, cost+tc);

	visited[i] = 0;
    }

    // if all visited

    if (flag == 0) {
	cost += m[start][start_org];
	if (cost < min_cost || min_cost == 0)
	    min_cost = cost;
    }
}

int main(void)
{
    min_cost = 0;
    int **m;
    int c;

    scanf("%d", &c);

    m = (int **) malloc(sizeof(int *) * c);

    int i, j;
    for(i = 0; i < c; i++) {
	m[i] = (int *) malloc(sizeof(int) * c);

	for(j = 0; j < c; j++) {
	    scanf("%d", &m[i][j]);
	    printf("%d ", m[i][j]);
	}
	printf("\n");
    }

    visited = (int *) malloc(sizeof(int) * c);

    int s;
    scanf("%d", &s);
    start_org = s;
    f(m, c, s, 0);

    printf("Min Cost %d\n", min_cost);
    return 0;
}

