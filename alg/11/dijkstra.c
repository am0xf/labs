#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

typedef struct {
    int i, d, p;
} vert;

int in(vert *l, int n, int c)
{
    while (n) {
	if (l[n-1].i == c)
	    return 1;
	n--;
    }
    return 0;
}

void setd(vert *q, int i, int d)
{
    int j;
    for (j = 0; ; j++)
	if (q[j].i == i)
	    break;

    q[j].d = d;
}

vert delmin(vert *q, int len)
{
    int m = 0;
    int i;
    for (i = 1; i < len; i++)
	if (q[i].d < q[m].d)
	    m = i;

    vert t = q[m];

    for (i = m; i < len - 1; i++)
	q[i] = q[i+1];

    return t;
}

int getd(vert *q, int id)
{
    int i;
    for (i = 0; ; i++)
	if (q[i].i == id)
	    return q[i].d;
}

void setp(vert *q, int i, int p)
{
    int j;
    for (j = 0;; j++)
	if (q[j].i == i)
	    break;
    q[j].p = p;
}

int slt(int a, int b, int c)
{
    if (a == INT_MAX)
	return 0;

    return (a + b) < c;
}

void show(vert *q, int n)
{
    int i;
    printf("\n");
    for (i = 0; i < n; i++)
	printf("%d %d %d\n", q[i].i, q[i].d, q[i].p);
    printf("\n");
}

vert* dijkstra(int **am, int n, int s)
{
    vert *list = (vert *) malloc(sizeof(vert) * n);
    vert *q = (vert *) malloc(sizeof(vert) * n);
    int qn = n;
    int i, j;

    for (i = 0; i < n; i++) {
	q[i].i = i+1;
	q[i].d = INT_MAX;
	q[i].p = -1;
    }

    setd(q, s, 0);

    vert t;
    int lp = 0;

    for (i = 1; i <= n; i++) {
	t = delmin(q, qn);
	qn--;

	list[lp++] = t;

	for (j = 1; j <= n; j++) {
	    if (j == t.i || am[t.i][j] == -1 || in(list, lp, j))
		continue;

	    if (slt(t.d, am[t.i][j], getd(q, j))) {
		setd(q, j, t.d + am[t.i][j]);
		setp(q, j, t.i);
	    }
	}
    }

    return list;
}

int main(void)
{
    int n, e;

    scanf("%d %d", &n, &e);

    int **am = (int **) malloc(sizeof(int*) * (n+1));

    int i, j;
    for (i = 1; i <= n; i++) {
	am[i] = (int *) malloc(sizeof(int) * (n+1));
	for (j = 1; j <= n; j++)
	   am[i][j] = -1; 
    }

    int u, v, w;
    for (i = 0; i < e; i++) {
	scanf("%d %d %d", &u, &v, &w);
	am[u][v] = w;
	am[v][u] = w;
    }

    int s;
    scanf("%d", &s);

    vert *list = dijkstra(am, n, s);

    printf("\n");
    for (i = 0; i < n; i++)
	printf("%d %d %d\n", list[i].i, list[i].p, list[i].d);
    printf("\n");

    return 0;
}

