#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int u, v, w;
} edge;

void sort(edge *list, int len)
{
    int i, j;
    edge t;
    
    for (i = 0; i < len; i++) {
	for (j = 0; j < len - i - 1; j++) {
	    if (list[j].w > list[j+1].w) {
		t = list[j];
		list[j] = list[j+1];
		list[j+1] = t;
	    }
	}
    }
}

int in(int *l, int n, int c)
{
    while (n) {
	if (l[n-1] == c)
	    return 1;
	n--;
    }
    return 0;
}

int check_acyclic(edge *list, int len)
{
    int *vl = (int *) malloc(sizeof(int) * (len * 2));
    int c = 0;

    int i;
    for (i = 0; i < len; i++) {
	if (!in(vl, c, list[i].u))
	    vl[c++] = list[i].u;
	if (!in(vl, c, list[i].v))
	    vl[c++] = list[i].v;
	if (i+1 >= c) {
	    free(vl);
	    return 0;
	}
    }
    
    free(vl);

    return 1;
}

int kruskal(edge *spt, edge *list, int len)
{
    sort(list, len);

    int c = 0, d = 0, i;

    while (c < len - 1) {
	spt[c++] = list[d++];
	if (!check_acyclic(spt, c))
	    c--;
    }

    return c;
}

int main(void)
{
    int n;

    scanf("%d", &n);

    edge *list = (edge *) malloc(sizeof(edge) * n);
    edge *spt = (edge *) malloc(sizeof(edge) * n);

    int i;
    for (i = 0; i < n; i++)
	scanf("%d %d %d", &list[i].u, &list[i].v, &list[i].w);

    n = kruskal(spt, list, n);

    printf("\n");
    for (i = 0; i < n; i++)
	printf("%d %d %d\n", spt[i].u, spt[i].v, spt[i].w);
    printf("\n");

    return 0;
}

