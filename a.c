#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <mpi.h>

int main(void)
{
    MPI_Init(NULL, NULL);

    MPI_Status st;
    int stt = 1;

    int mat[16], m[4], nl;
    char *str, *s;
    str = (char *) malloc(sizeof(char) * 81);

    int rk, sz = 4;

    MPI_Comm_rank(MPI_COMM_WORLD, &rk);

    if (!rk) {
	int i, j;
	fgets(str, 80, stdin);

	int l = strlen(str) - 1;
	nl = l / 4;

	for (i = 0; i < 4; i++)
	    for(j = 0; j < 4; j++)
		scanf("%d", &mat[4*j+i]); // opposite indexing for easier scattering
    }

    // Bcast partial string length
    MPI_Bcast(&nl, 1, MPI_INT, 0, MPI_COMM_WORLD);

    s = (char *) malloc(sizeof(char) * nl);

    MPI_Scatter(str, nl, MPI_CHAR, s, nl, MPI_CHAR, 0, MPI_COMM_WORLD);
    
    // Scatter columns
    MPI_Scatter(mat, 4, MPI_INT, m, 4, MPI_INT, 0, MPI_COMM_WORLD);

    int sm = 0, mn = m[0];

    int i;

    for (i = 0; i < nl; i++) {
	if (isdigit(s[i]))
	    sm += s[i] - 48;
    }

    for (i = 1; i < 4; i++) {
	if (m[i] < mn)
	    mn = m[i];
    }

    printf("Rank : %d, Sum : %d, Min + Sum : %d\n", rk, sm, sm + mn);

    MPI_Finalize();

    return 0;
}

