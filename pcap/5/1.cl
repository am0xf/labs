__kernel void conv(__global int *a)
{
	int pos = 1, m = 0;
	int id = get_global_id(0);
	int n = a[id];
	
	while (n) {
		m += (n % 8) * pos;
		pos *= 10;
		n /= 8;
	}
	
	a[id] = m;
}
