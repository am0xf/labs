__kernel void comp(__global int *a)
{
	int id = get_global_id(0);
	int n = a[id], m = 0;
	int pos = 1;
	
	if (n) {
		while (n) {
			m += pos * ((n % 2) ^ 1);
			pos *= 2;
			n /= 2;
		}
	}
	else {
		m = 1;
	}
	
	a[id] = m;
}
