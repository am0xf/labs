__kernel void swap(__global int *a)
{
	int id = get_global_id(0), n;
	
	if (id % 2) {
		n = a[id-1];
		a[id-1] = a[id];
		a[id] = n;
	}
}
