#include <cuda_runtime.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB 1
#define NTH 1
#define N NB * NTH

__global__ void countfreq(char *a, char *b, int *n)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;

    int i, ws = 0;
    for (i = 0; ws < id; i++)
	if (a[i] == ' ')
	    ws++;

    int st = i, sp = i;

    for (; a[sp] != ' ' && a[sp]; sp++);

    int l = sp - st;

    for (i = 0; i < l && b[i]; i++)
	if (a[st+i] != b[i])
	    break;

    if (i == l)
	atomicAdd(n, 1);
}

int main(void)
{
    char *a = (char *) malloc(sizeof(char) * 80);
    char *b = (char *) malloc(sizeof(char) * 80);

    char *d_a, *d_b;
    int *d_n;

    fgets(a, 79, stdin);
    fgets(b, 79, stdin);
    int l = strlen(a) - 1;
    int n = 0;

    cudaMalloc((void **) &d_a, sizeof(char) * l);
    cudaMalloc((void **) &d_b, sizeof(char) * l);
    cudaMalloc((void **) &d_n, sizeof(int));


    cudaMemcpy(d_a, a, sizeof(char) * l, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, sizeof(char) * l, cudaMemcpyHostToDevice);
    cudaMemcpy(d_n, &n, sizeof(int), cudaMemcpyHostToDevice);

    int ws = 0;
    int i;
    for (i = 0; i < l; i++)
	if (a[i] == ' ')
	    ws++;

    countfreq<<<NB,ws+1>>>(d_a, d_b, d_n);

    cudaMemcpy(&n, d_n, sizeof(int), cudaMemcpyDeviceToHost);

    printf("%d\n", n);

    return 0;
}

