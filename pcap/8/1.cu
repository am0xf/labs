#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB 1
#define NTH 5
#define N NB * NTH

__global__ void repeat(char *a, char *b, int *n)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    int st = *n * id;

    int i;
    for (i = 0; a[i]; i++)
	b[st+i] = a[i];
}

int main(void)
{
    char *a = (char *) malloc(sizeof(char) * 80);
    char *b = (char *) malloc(sizeof(char) * 80);

    char *d_a, *d_b;
    int *d_n;

    fgets(a, 79, stdin);

    int l = strlen(a) - 1;

    cudaMalloc((void **) &d_a, sizeof(char) * l);
    cudaMalloc((void **) &d_b, sizeof(char) * l * N);
    cudaMalloc((void **) &d_n, sizeof(int));

    cudaMemcpy(d_a, a, sizeof(char) * l, cudaMemcpyHostToDevice);
    cudaMemcpy(d_n, &l, sizeof(int), cudaMemcpyHostToDevice);

    repeat<<<NB,NTH>>>(d_a, d_b, d_n);

    cudaMemcpy(b, d_b, sizeof(char) * N * l, cudaMemcpyDeviceToHost);

    b[l*N] = '\n';
    b[l*N+1] = '\0';
    fputs(b, stdout);

    return 0;
}

