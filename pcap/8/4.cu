#include <cuda_runtime.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB 1
#define NTH 1
#define N NB * NTH

__global__ void revsrt_wordwise(char *a, char *b, int *n)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;

    int i, ws = 0;
    for (i = 0; ws < id; i++)
	if (a[i] == ' ')
	    ws++;

    int st = i, sp = i;

    for (; a[sp] != ' ' && a[sp]; sp++);

    int l = sp - st;

    for (i = 0; i < l; i++)
	b[*n-1-st-i] = a[st+i];
}

int main(void)
{
    char *a = (char *) malloc(sizeof(char) * 80);
    char *b = (char *) malloc(sizeof(char) * 80);

    char *d_a, *d_b;
    int *d_n;

    fgets(a, 79, stdin);
    int l = strlen(a) - 1;

    cudaMalloc((void **) &d_a, sizeof(char) * l);
    cudaMalloc((void **) &d_b, sizeof(char) * l);
    cudaMalloc((void **) &d_n, sizeof(int));


    cudaMemcpy(d_a, a, sizeof(char) * l, cudaMemcpyHostToDevice);
    cudaMemcpy(d_n, &l, sizeof(int), cudaMemcpyHostToDevice);

    int ws = 0;
    int i;
    for (i = 0; i < l; i++)
	if (a[i] == ' ')
	    ws++;

    countfreq<<<NB,ws+1>>>(d_a, d_b, d_n);

    cudaMemcpy(b, d_b, sizeof(char) * l, cudaMemcpyDeviceToHost);

    b[l] = '\n';
    b[l+1] = '\0';

    fputs(b, stdout);

    return 0;
}

