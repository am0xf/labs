#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB 5
#define NTH 1
#define N NB * NTH

__global__ void revword(char *a)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;

    int i, ws = 0;
    for (i = 0; ws < id; i++)
	if (a[i] == ' ')
	    ws++;

    int st = i, sp = i;

    for (; a[sp] != ' ' && a[sp]; sp++);

    int l = sp - st;

    char s;
    for (i = 0; i < l / 2; i++) {
	s = a[st+i];
	a[st+i] = a[sp-i-1];
	a[sp-i-1] = s;
    }
}

int main(void)
{
    char *a = (char *) malloc(sizeof(char) * 80);

    char *d_a;

    fgets(a, 79, stdin);
    int l = strlen(a) - 1;

    cudaMalloc((void **) &d_a, sizeof(char) * l);

    cudaMemcpy(d_a, a, sizeof(char) * l, cudaMemcpyHostToDevice);

    int ws = 0;
    int i;
    for (i = 0; i < l; i++)
	if (a[i] == ' ')
	    ws++;

    revword<<<ws+1,NTH>>>(d_a);

    cudaMemcpy(a, d_a, sizeof(char) * l, cudaMemcpyDeviceToHost);

    fputs(a, stdout);

    return 0;
}

