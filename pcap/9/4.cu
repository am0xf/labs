#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB 1
#define NTH 5
#define N NB * NTH

__global__ void add(int *a, int *b)
{
    int col = blockIdx.x * blockDim.x + threadIdx.x;
    int row = blockIdx.y * blockDim.y + threadIdx.y;

    a[row * blockDim.x + col] += b[row * blockDim.x + col];
}

int main(void)
{
    int m, n;
    scanf("%d %d", &m, &n);

    int *a = (int *) malloc(sizeof(int) * n * m);
    int *b = (int *) malloc(sizeof(int) * n * m);

    int i, j;
    for (i = 0; i < m; i++)
	for (j = 0; j < n; j++)
	    scanf("%d %d", a+n*i+j, b+n*i+j);

    int *d_a, *d_b;

    cudaMalloc((void **) &d_a, sizeof(int) * n * m);
    cudaMalloc((void **) &d_b, sizeof(int) * n * m);

    cudaMemcpy(d_a, a, sizeof(int) * n * m, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, a, sizeof(int) * n * m, cudaMemcpyHostToDevice);

    dim3 thd_grid(n, m, 1);

    add<<<1, thd_grid>>>(d_a, d_b);

    cudaMemcpy(a, d_a, sizeof(int) * n * m, cudaMemcpyDeviceToHost);

    for (i = 0; i < m; i++) {
	for (j = 0; j < n; j++)
	    printf("%d ", *(a+n*i+j));
	printf("\n");
    }

    return 0;
}

