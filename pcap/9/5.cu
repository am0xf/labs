#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB 1
#define NTH 5
#define N NB * NTH

__global__ void mulrw(int *a, int *b, int *c, int *n, int *p)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;

    int i, j;
    for (i = 0; i < *p; i++) // cols in result
	for (j = 0; j < *n; j++)
	    c[id * *p + i] += a[id * *n + j] * b[j * *p + i];
}

int main(void)
{
    int m, n, p;
    scanf("%d %d %d", &m, &n, &p);

    int *a = (int *) malloc(sizeof(int) * m * n);
    int *b = (int *) malloc(sizeof(int) * n * p);
    int *c = (int *) malloc(sizeof(int) * m * p);

    int i, j;
    for (i = 0; i < m; i++)
	for (j = 0; j < n; j++)
	    scanf("%d", a+n*i+j);
    for (i = 0; i < n; i++)
	for (j = 0; j < p; j++)
	    scanf("%d", b+p*i+j);

    int *d_a, *d_b, *d_c, *d_n, *d_p;

    cudaMalloc((void **) &d_a, sizeof(int) * m * n);
    cudaMalloc((void **) &d_b, sizeof(int) * n * p);
    cudaMalloc((void **) &d_c, sizeof(int) * m * p);
    cudaMalloc((void **) &d_n, sizeof(int));
    cudaMalloc((void **) &d_p, sizeof(int));

    cudaMemcpy(d_a, a, sizeof(int) * m * n, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, a, sizeof(int) * n * p, cudaMemcpyHostToDevice);
    cudaMemcpy(d_n, &n, sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_p, &p, sizeof(int), cudaMemcpyHostToDevice);

    mulrw<<<1, m>>>(d_a, d_b, d_c, d_n, d_p);

    cudaMemcpy(c, d_c, sizeof(int) * m * p, cudaMemcpyDeviceToHost);

    for (i = 0; i < m; i++) {
	for (j = 0; j < p; j++)
	    printf("%d ", *(c+p*i+j));
	printf("\n");
    }

    return 0;
}

