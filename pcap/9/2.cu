#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB 1
#define NTH 5
#define N NB * NTH

__global__ void addrw(int *a, int *b, int *n)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;

    int i;
    for (i = 0; i < *n; i++)
	a[id * *n + i] += b[id * *n + i];
}

int main(void)
{
    int m, n;
    scanf("%d %d", &m, &n);

    int *a = (int *) malloc(sizeof(int) * n * m);
    int *b = (int *) malloc(sizeof(int) * n * m);

    int i, j;
    for (i = 0; i < m; i++)
	for (j = 0; j < n; j++)
	    scanf("%d %d", a+n*i+j, b+n*i+j);

    int *d_a, *d_b, *d_n;

    cudaMalloc((void **) &d_a, sizeof(int) * n * m);
    cudaMalloc((void **) &d_b, sizeof(int) * n * m);
    cudaMalloc((void **) &d_n, sizeof(int));

    cudaMemcpy(d_a, a, sizeof(int) * n * m, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, a, sizeof(int) * n * m, cudaMemcpyHostToDevice);
    cudaMemcpy(d_n, &n, sizeof(int), cudaMemcpyHostToDevice);

    addrw<<<1, m>>>(d_a, d_b, d_n);

    cudaMemcpy(a, d_a, sizeof(int) * n * m, cudaMemcpyDeviceToHost);

    for (i = 0; i < m; i++) {
	for (j = 0; j < n; j++)
	    printf("%d ", *(a+n*i+j));
	printf("\n");
    }

    return 0;
}

