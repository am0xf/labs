#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB 1
#define NTH 5
#define N NB * NTH

__global__ void morph(int *mat, int *n)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;

    int i, j;
    for (i = id * *n; i < id * (*n + 1); i++)
	for (j = 0; j < id; j++)
	    mat[i] *= mat[i];
}

int main(void)
{
    int m, n;
    scanf("%d %d", &m, &n);

    int *a = (int *) malloc(sizeof(int) * n * m);

    int i, j;
    for (i = 0; i < m; i++)
	for (j = 0; j < n; j++)
	    scanf("%d", a+n*i+j);

    int *d_m, *d_n;

    cudaMalloc((void **) &d_m, sizeof(int) * n * m);
    cudaMalloc((void **) &d_n, sizeof(int));

    cudaMemcpy(d_m, a, sizeof(int) * n * m, cudaMemcpyHostToDevice);
    cudaMemcpy(d_n, &n, sizeof(int), cudaMemcpyHostToDevice);

    morph<<<1, m>>>(d_m, d_n);

    cudaMemcpy(a, d_m, sizeof(int) * n * m, cudaMemcpyDeviceToHost);

    for (i = 0; i < m; i++) {
	for (j = 0; j < n; j++)
	    printf("%d ", *(a+n*i+j));
	printf("\n");
    }

    return 0;
}

