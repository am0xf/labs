#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB 1
#define NTH 5
#define N NB * NTH

__global__ void morph(int *a)
{
    int col = blockIdx.x * blockDim.x + threadIdx.x;
    int row = blockIdx.y * blockDim.y + threadIdx.y;

    if (row && col && row != blockDim.y - 1 && col != blockDim.x - 1)
	a[row * blockDim.x + col] = ~a[row * blockDim.x + col];
}

int main(void)
{
    int m, n;
    scanf("%d %d", &m, &n);

    int *a = (int *) malloc(sizeof(int) * n * m);

    int i, j;
    for (i = 0; i < m; i++)
	for (j = 0; j < n; j++)
	    scanf("%d", a+n*i+j);

    int *d_a;

    cudaMalloc((void **) &d_a, sizeof(int) * n * m);

    cudaMemcpy(d_a, a, sizeof(int) * n * m, cudaMemcpyHostToDevice);

    dim3 thd_grid(n, m, 1);

    morph<<<1, thd_grid>>>(d_a);

    cudaMemcpy(a, d_a, sizeof(int) * n * m, cudaMemcpyDeviceToHost);

    for (i = 0; i < m; i++) {
	for (j = 0; j < n; j++)
	    printf("%d ", *(a+n*i+j));
	printf("\n");
    }

    return 0;
}

