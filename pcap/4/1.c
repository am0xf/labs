#include "headers.h"

int main(int argc, char *argv[])
{
    MIn(&argc, &argv);

    int rank, size, n, r, err_len;
    int *buf;
    int i;
    int s = 0;
    int err;
    char err_str[80];

    MSt status;

    MCr(MPI_COMM_WORLD, &rank);
    MCs(MPI_COMM_WORLD, &size);

    MEhs(MCW, MER);

    buf = (int *) malloc(sizeof(int) * size);

    if (!rank) {
	for (i = 0; i < size; i++)
	    buf[i] = i + 1;
    }

    err = MSc(buf, 1, MI, &n, 1, MI, 0, MCW);
    MErs(err, err_str, &err_len);

    err = MSn(&n, &r, 1, MPI_INT, MPI_PROD, MCW);
    MErs(err, err_str, &err_len);

    err = MRe(&r, &s, 1, MI, MPI_SUM, 0, MCW);
    MErs(err, err_str, &err_len);

    if (!rank) {
	printf("%d\n", s);
    }

    MF();

    return 0;
}

