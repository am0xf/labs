#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <ctype.h>
#include <string.h>

#define MALLOC(size, type) ((type) *) malloc(sizeof((type)) * (size))

#define MIn MPI_Init
#define MCW MPI_COMM_WORLD
#define MCr MPI_Comm_rank
#define MCs MPI_Comm_size
#define MI MPI_INT
#define MD MPI_DOUBLE
#define MC MPI_CHAR
#define MSc MPI_Scatter
#define MSt MPI_Status
#define MGa MPI_Gather
#define MBc MPI_Bcast
#define MSn MPI_Scan
#define MRe MPI_Reduce
#define MSM MPI_SUM
#define MPD MPI_PROD
#define MMN MPI_MAX
#define MMX MPI_MIN
#define MEhs MPI_Errhandler_set
#define MErs MPI_Error_string
#define MER MPI_ERRORS_RETURN
#define MF MPI_Finalize

