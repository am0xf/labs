#include "headers.h"

int main(int argc, char *argv[])
{
    MIn(&argc, &argv);

    int rank, size, n, err_len, a, b;
    int *buf;
    int i;
    double s = 0;
    int err;
    char err_str[80];
    double r, d;

    MSt status;

    MCr(MCW, &rank);
    MCs(MCW, &size);

    MEhs(MCW, MER);

	b = 1;
	a = 0;
	d = (b - a) / (double) size;

    r = 4 / (1 + (rank * d) * (rank * d)) * d;

    err = MRe(&r, &s, 1, MD, MPI_SUM, 0, MCW);
    MErs(err, err_str, &err_len);

    if (!rank) {
	printf("%.5f\n", s);
    }

    MF();

    return 0;
}

