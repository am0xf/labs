#include "headers.h"

int main(int argc, char *argv[])
{
    MIn(&argc, &argv);

    int rank, size, err_len, recv[4], recv2[4], sr, s, i, j;
    int mat[4][4];
    int err;
    int occ = 0;
    char err_str[80];

    MSt status;

    MCr(MPI_COMM_WORLD, &rank);
    MCs(MPI_COMM_WORLD, &size);

    MEhs(MCW, MER);

    if (!rank) {
	for (i = 0; i < 4; i++)
	    for (j = 0; j < 4; j++)
		scanf("%d", &mat[i][j]);
	printf("\n");
    }
    
    err = MSc(mat, 4, MI, recv, 4, MI, 0, MCW);
    MErs(err, err_str, &err_len);

    err = MSn(recv, recv2, 4, MI, MPI_SUM, MCW);
    MErs(err, err_str, &err_len);

    err = MGa(recv2, 4, MI, mat, 4, MI, 0, MCW);
    MErs(err, err_str, &err_len);
    
    if (!rank) {
	for (i = 0; i < 4; i++) {
	    for (j = 0; j < 4; j++)
		printf("%d ", mat[i][j]);
	    printf("\n");
	}
    }

    MF();

    return 0;
}

