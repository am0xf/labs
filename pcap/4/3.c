#include "headers.h"

int main(int argc, char *argv[])
{
    MIn(&argc, &argv);

    int rank, size, err_len, recv[3], sr, s, i, j;
    int mat[3][3];
    int err;
    int occ = 0;
    char err_str[80];

    MSt status;

    MCr(MPI_COMM_WORLD, &rank);
    MCs(MPI_COMM_WORLD, &size);

    MEhs(MCW, MER);

    if (!rank) {
	for (i = 0; i < 3; i++)
	    for (j = 0; j < 3; j++)
		scanf("%d", &mat[i][j]);

	scanf("%d", &sr);
    }

    err = MBc(&sr, 1, MI, 0, MCW);
    MErs(err, err_str, &err_len);

    for (i = 0; i < 3; i++) {
	err = MSc(&mat[i], 1, MI, &recv[i], 1, MI, 0, MCW);
	MErs(err, err_str, &err_len);
    }

    occ = 0;
    for (i = 0; i < 3; i++)
	if (recv[i] == sr)
	    occ++;

    err = MRe(&occ, &s, 1, MI, MPI_SUM, 0, MCW);
    MErs(err, err_str, &err_len);

    if (!rank) {
	printf("%d\n", s);
    }

    MF();

    return 0;
}

