__kernel void sel_sort(__global int *a, __global int *b)
{
	int id = get_global_id(0);
	int sz = get_global_size(0);
	int pos = 0;

	for (int i = 1; i < sz; i++)
	    if (a[i] < a[id] || a[i] == a[id] && i < id)
		pos++;

	b[pos] = a[id];
}
