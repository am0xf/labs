__kernel void replicate(__global char *s, __global int *l, __global char *r)
{
	int id = get_global_id(0);
	
	int start = *l * id;
	
	for (int i = 0; i < *l; i++)
		r[start++] = s[i];
}
