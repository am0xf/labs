#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>

#define MAX_SRC_SZ 0x100000
#define FILENAME "2.cl"
#define FUNC "sel_sort"
#define n_wrk 10

int main(void)
{
	int *a = (int *) malloc(sizeof(int) * n_wrk);
	int *b = (int *) malloc(sizeof(int) * n_wrk);
	int i;
	for (i = 0; i < n_wrk; i++)
		a[i] = n_wrk - i;

	FILE *fp = fopen(FILENAME, "r");
	char *src = (char *) malloc(MAX_SRC_SZ);
	size_t src_sz = fread(src, sizeof(char), MAX_SRC_SZ, fp);
	fclose(fp);

	cl_platform_id *pid = NULL;
	cl_device_id did;
	cl_uint r_n_dvs = 1, r_n_pfs = 1, ret;

	ret = clGetPlatformIDs(1, &pid, &r_n_pfs);

	ret = clGetDeviceIDs(pid, CL_DEVICE_TYPE_CPU, 1, &did, &r_n_dvs);

	cl_context cntxt = clCreateContext(NULL, 1, &did, NULL, NULL, &ret);
	cl_command_queue cmd_q = clCreateCommandQueue(cntxt, did, NULL, &ret);

	cl_mem arr = clCreateBuffer(cntxt, CL_MEM_READ_WRITE, n_wrk * sizeof(int), NULL, &ret);
	cl_mem arr2 = clCreateBuffer(cntxt, CL_MEM_READ_WRITE, n_wrk * sizeof(int), NULL, &ret);
	ret = clEnqueueWriteBuffer(cmd_q, arr, CL_TRUE, 0, n_wrk * sizeof(int), a, 0, NULL, NULL);

	cl_program prog = clCreateProgramWithSource(cntxt, 1, (const char **) &src, (const size_t *) &src_sz, &ret);
	ret = clBuildProgram(prog, 1, &did, NULL, NULL, NULL);

	cl_kernel krnl = clCreateKernel(prog, FUNC, &ret);

	size_t gis = n_wrk;
	size_t lis = 1;

	cl_event evnt;

	    ret = clSetKernelArg(krnl, 0, sizeof(cl_mem), (void *) &arr);
	    ret = clSetKernelArg(krnl, 1, sizeof(cl_mem), (void *) &arr2);
	    ret = clEnqueueNDRangeKernel(cmd_q, krnl, 1, NULL, &gis, &lis, 0, NULL, NULL);

	    ret = clFinish(cmd_q);

	ret = clEnqueueReadBuffer(cmd_q, arr2, CL_TRUE, 0, sizeof(int) * n_wrk, b, 0, NULL, NULL);

	for (i = 0; i < n_wrk; i++)
		printf("%d\n", b[i]);

	ret = clFlush(cmd_q);
	ret = clReleaseKernel(krnl);
	ret = clReleaseProgram(prog);
	ret = clReleaseMemObject(arr);
	ret = clReleaseCommandQueue(cmd_q);
	ret = clReleaseContext(cntxt);

	return 0;
}
