__kernel void odd_even(__global int *a, int ph)
{
	int id = get_global_id(0);
	int t;

	if (id % 2 == ph && id < get_global_size(0) - 1) {
	    if (a[id] > a[id+1]) {
		t = a[id];
		a[id] = a[id+1];
		a[id+1] = t;
	    }
	}
}
