#include <stdio.h>

#include "mpi.h"

int prime(int n)
{
    int i;
    for(i = 2; i <= n / 2; i++)
	if (!(n % i))
	    return 0;

    return 1;
}

int main(int argc, char *argv[])
{
    int rank;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int i;
    for(i = 1 + 50 * rank; i < 50 + 50 * rank; i++)
	if (prime(i))
	    printf("%d : %d\n", rank, i);

    MPI_Finalize();

    return 0;
}

