#include <stdio.h>

#include "mpi.h"

#define A 500
#define B 4

int main(int argc, char *argv[])
{
    int rank, a = A, b = B;


    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   
    switch (rank)
    {
	case 0:
	    printf("%d\n", a + b);
	    break;

	case 1:
	    printf("%d\n", a - b);
	    break;

	case 2:
	    printf("%d\n", a * b);
	    break;

	case 3:
	    printf("%d\n", a / b);
	    break;

	case 4:
	    printf("%d\n", a % b);
	    break;
    }

    MPI_Finalize();

    return 0;
}

