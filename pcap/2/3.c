#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <ctype.h>

int main(int argc, char *argv[])
{
    int *ar, n, *buf;
    int i;

    MPI_Init(&argc, &argv);

    int bsize = 96, size;
    buf = (int *) malloc(bsize);
    MPI_Buffer_attach(buf, bsize);

    int rank;
    MPI_Status status;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (!rank) {
	ar = (int *) malloc(size - 1);

	printf("Enter %d numbers\n", size - 1);

	for (i = 0; i < size - 1; i++)
	    scanf("%d", &ar[i]);

	for (i = 1; i <= size - 1; i++)
	    MPI_Bsend(&ar[i-1], 1, MPI_INT, i, i, MPI_COMM_WORLD);
	    
    }
    else {
	MPI_Recv(&n, 1, MPI_INT, 0, rank, MPI_COMM_WORLD, &status);
	
	if (rank % 2)
	    n = n * n;
	else
	    n = n * n * n;

	printf("Rank %d : %d\n", rank, n);
    }

    MPI_Buffer_detach(buf, &bsize);

    MPI_Finalize();

    return 0;
}

