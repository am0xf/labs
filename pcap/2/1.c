#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <ctype.h>

int main(int argc, char *argv[])
{
    char *word = (char *) malloc(80);

    MPI_Init(&argc, &argv);

    int rank;
    MPI_Status status;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (!rank) {
	fgets(word, 79, stdin);

	MPI_Ssend(word, 80, MPI_CHAR, 1, 1, MPI_COMM_WORLD);

	MPI_Recv(word, 80, MPI_CHAR, 1, 2, MPI_COMM_WORLD, &status);

	fputs(word, stdout);
    }
    else {
	MPI_Recv(word, 80, MPI_CHAR, 0, 1, MPI_COMM_WORLD, &status);

	int i;
	for (i = 0; i < 80; i++)
	    if (isupper(word[i]))
		word[i] = tolower(word[i]);
	   else if (islower(word[i]))
		word[i] = toupper(word[i]);

	MPI_Ssend(word, 80, MPI_CHAR, 0, 2, MPI_COMM_WORLD);
    }

    MPI_Finalize();

    return 0;
}

