#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <ctype.h>

int fact(int n)
{
	return (n == 1) ? 1 : n * fact(n-1);
}

#define sum(n) ((n) * ((n) + 1)) / 2

int main(int argc, char *argv[])
{
    int f = 1, s = 0, size;

    MPI_Init(&argc, &argv);

    int rank;
    MPI_Status status;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

	if (rank % 2 && rank > 0) {
		f = fact(rank + 1);
		MPI_Send(&f, 1, MPI_INT, 0, rank, MPI_COMM_WORLD);
	}
	else {
		s = sum(rank+1);
		MPI_Send(&s, 1, MPI_INT, 0, rank, MPI_COMM_WORLD);
	}

	if (!rank) {
		int fs = 1, t;

		int i;
		for (i = 1; i < size; i++) {
			MPI_Recv(&t, 1, MPI_INT, i, i, MPI_COMM_WORLD, &status);
			fs += t;
		}

		printf("%d\n", fs);
	}

    MPI_Finalize();

    return 0;
}

