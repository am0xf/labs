#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <ctype.h>

int main(int argc, char *argv[])
{
    int n;

    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Status status;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (!rank) {
	scanf("%d", &n);

	int i;
	for (i = 1; i < size; i++)
	    MPI_Send(&n, 1, MPI_INT, i, i, MPI_COMM_WORLD);
    }
    else {
	MPI_Recv(&n, 1, MPI_INT, 0, rank, MPI_COMM_WORLD, &status);
	printf("Rank %d : %d\n", rank, n);
    }

    MPI_Finalize();

    return 0;
}

