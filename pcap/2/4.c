#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <ctype.h>

int main(int argc, char *argv[])
{
    int n, size;

    MPI_Init(&argc, &argv);

    int rank;
    MPI_Status status;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (!rank) {
	scanf("%d", &n);
	n++;
	MPI_Send(&n, 1, MPI_INT, 1, 1, MPI_COMM_WORLD);
	MPI_Recv(&n, 1, MPI_INT, size - 1, size, MPI_COMM_WORLD, &status);
	printf("%d\n", n);
    }
    else {
	MPI_Recv(&n, 1, MPI_INT, rank - 1, rank, MPI_COMM_WORLD, &status);
	n++;
	MPI_Send(&n, 1, MPI_INT, rank == size - 1 ? 0 : rank + 1, rank + 1, MPI_COMM_WORLD);
    }

    MPI_Finalize();

    return 0;
}

