#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NB 1
#define NTH 5
#define N NB * NTH

__global__ void convolution(int *a, int *b, int *l, int *m, int *n)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;

    int start = id - (*n - 1) / 2;
    int stop = (start + *n > *l) ? start + *n - *l : *n;
    int i;
    for (i = (start < 0) ? -start : 0; i < stop; i++)
        b[id] += a[start + i] * m[i]
}

int main(void)  // incomplete
{
    int m, n;
    scanf("%d %d", &n, &m);

    int *a = (int *) malloc(sizeof(int) * n);
    int *b = (int *) malloc(sizeof(int) * n);
    int *mk = (int *) malloc(sizeof(int) * m);
    
    int pad = (n - 1) / 2;

    int i, j;
    for (i = 0; i < m; i++)
	for (j = 0; j < n; j++)
	    scanf("%d", a+n*i+j);

    int *d_m, *d_n;

    cudaMalloc((void **) &d_m, sizeof(int) * n * m);
    cudaMalloc((void **) &d_n, sizeof(int));

    cudaMemcpy(d_m, a, sizeof(int) * n * m, cudaMemcpyHostToDevice);
    cudaMemcpy(d_n, &n, sizeof(int), cudaMemcpyHostToDevice);

    morph<<<1, m>>>(d_m, d_n);

    cudaMemcpy(a, d_m, sizeof(int) * n * m, cudaMemcpyDeviceToHost);

    for (i = 0; i < m; i++) {
	for (j = 0; j < n; j++)
	    printf("%d ", *(a+n*i+j));
	printf("\n");
    }

    return 0;
}

