#include <stdio.h>
#include <stdlib.h>

#define NB 1
#define NTH 5

__global__ void add(int *a, int *b, int *c)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;

    c[id] = a[id] + b[id];
}

int main(void)
{
    int *a = (int *) malloc(sizeof(int) * NB * NTH);
    int *b = (int *) malloc(sizeof(int) * NB * NTH);
    int *c = (int *) malloc(sizeof(int) * NB * NTH);

    int *d_a, *d_b, *d_c;

    cudaMalloc((void **) &d_a, sizeof(int) * NB * NTH);
    cudaMalloc((void **) &d_b, sizeof(int) * NB * NTH);
    cudaMalloc((void **) &d_c, sizeof(int) * NB * NTH);

    int i;
    for (i = 0; i < NB * NTH; i++)
	a[i] = NB * NTH - i;
    for (i = 0; i < NB * NTH; i++)
	b[i] = NB * NTH - i;

    cudaMemcpy(d_a, a, sizeof(int) * NB * NTH, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, sizeof(int) * NB * NTH, cudaMemcpyHostToDevice);

    add<<<NB,NTH>>>(d_a, d_b, d_c);

    cudaMemcpy(c, d_c, sizeof(int) * NB * NTH, cudaMemcpyDeviceToHost);

    for (i = 0; i < NB * NTH; i++)
	printf("%d\n", c[i]);

    return 0;
}

