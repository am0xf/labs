#include <stdio.h>
#include <stdlib.h>

#define NB 1
#define NTH 5

__global__ void add(double *a, double *b)
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    b[id] = sin(a[id]);
}

int main(void)
{
    double *a = (double *) malloc(sizeof(double) * NB * NTH);
    double *b = (double *) malloc(sizeof(double) * NB * NTH);

    double *d_a, *d_b;

    cudaMalloc((void **) &d_a, sizeof(double) * NB * NTH);
    cudaMalloc((void **) &d_b, sizeof(double) * NB * NTH);

    int i;
    for (i = 0; i < NB * NTH; i++)
	a[i] = 3.14 + i;

    cudaMemcpy(d_a, a, sizeof(double) * NB * NTH, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b, sizeof(double) * NB * NTH, cudaMemcpyHostToDevice);

    add<<<NB,NTH>>>(d_a, d_b);

    cudaMemcpy(b, d_b, sizeof(double) * NB * NTH, cudaMemcpyDeviceToHost);

    for (i = 0; i < NB * NTH; i++)
	printf("%.5f\n", b[i]);

    return 0;
}

