#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"
#include <ctype.h>

void count(int *ar, int n, int *ne, int *no)
{
    while (n--) {
	ar[n] = ar[n] % 2? 0 : 1;
	ar[n] % 2? (*no)++ : (*ne)++;
    }
}

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int rank, size, n, *res_buf, l, il, *nen, *non;
    int *sbuf, *rbuf;
    int i, j;
    int ne = 0, no = 0;

    MPI_Status status;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    non = (int *) malloc(sizeof(int) * size);
    nen = (int *) malloc(sizeof(int) * size);

    if (!rank) {
	scanf("%d", &n);
	res_buf = (int *) malloc(sizeof(int) * n);
	sbuf = (int *) malloc(sizeof(int) * n);

	for (i = 0; i < n; i++)
	    sbuf[i] = i * i;
	
	il = n / size;
    }

    MPI_Bcast(&il, 1, MPI_INT, 0, MPI_COMM_WORLD);
    rbuf = (int *) malloc(sizeof(int) * il);

    MPI_Scatter(sbuf, il, MPI_INT, rbuf, il, MPI_INT, 0, MPI_COMM_WORLD);

    count(rbuf, il, &ne, &no);

    MPI_Gather(rbuf, il, MPI_INT, res_buf, il, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather(&ne, 1, MPI_INT, nen, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather(&no, 1, MPI_INT, non, 1, MPI_INT, 0, MPI_COMM_WORLD);

    if (!rank) {
	for (i = 0; i < n; i++)
	    printf("%d\n", res_buf[i]);

	int s = 0, p = 0;
	for (i = 0; i < size; i++) {
	    s += nen[i];
	    p += non[i];
	}

	printf("%d %d\n", s, p);

    }

    MPI_Finalize();

    return 0;
}

