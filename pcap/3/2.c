#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <ctype.h>

int sum(int *ar, int n)
{
    int s = 0;
    while (n--)
	s += ar[n];

    return s;
}

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int rank, size, n, m;
    int *sbuf, *rbuf;
    double *res_buf;
    double s2;
    int i, j;
    int s = 0;

    MPI_Status status;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    res_buf = (double *) malloc(sizeof(double) * size);

    if (!rank) {
	scanf("%d", &m);
	sbuf = (int *) malloc(sizeof(int) * size * m);
	for (i = 0; i < size; i++) {
	    for (j = 0; j < m; j++)
		sbuf[m * i + j] = (i+1) + (j+1);
	}
    }

    MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
    rbuf = (int *) malloc(sizeof(int) * m);

    MPI_Scatter(sbuf, m, MPI_INT, rbuf, m, MPI_INT, 0, MPI_COMM_WORLD);

    s = sum(rbuf, m);
    double avg = (double) s / m;

    MPI_Gather(&avg, 1, MPI_DOUBLE, res_buf, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    if (!rank) {
	for (i = 0; i < size; i++)
	    s2 += res_buf[i];

	printf("%.2f\n", s2 / size);
    }

    MPI_Finalize();

    return 0;
}

