#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"
#include <ctype.h>

void count(int *ar)
{
    int i;
    for (i = 1; i < 4; i++)
	ar[i] += ar[i-1];
}

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int rank, size, n, *res_buf;
    int *sbuf, *rbuf;

    res_buf = (int *) malloc(sizeof(int) * 16);
    sbuf = (int *) malloc(sizeof(int) * 16);
    rbuf = (int *) malloc(sizeof(int) * 4);

    int i, j;

    MPI_Status status;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (!rank) {
	sbuf[0] = 1;
	sbuf[1] = 2;
	sbuf[2] = 3;
	sbuf[3] = 5;
	sbuf[4] = 0;
	sbuf[5] = 3;
	sbuf[6] = 8;
	sbuf[7] = 7;
	sbuf[8] = 2;
	sbuf[9] = 0;
	sbuf[10] = 1;
	sbuf[11] = 7;
	sbuf[12] = 2;
	sbuf[13] = 3;
	sbuf[14] = 0;
	sbuf[15] = 9;
    }

    MPI_Scatter(sbuf, 4, MPI_INT, rbuf, 4, MPI_INT, 0, MPI_COMM_WORLD);

    count(rbuf);

    MPI_Gather(rbuf, 4, MPI_INT, res_buf, 4, MPI_INT, 0, MPI_COMM_WORLD);

    if (!rank) {
	for (i = 0; i < 16; i++)
	    printf("%d\n", res_buf[i]);
    }

    MPI_Finalize();

    return 0;
}

