#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"
#include <ctype.h>

int nov(char *ar, int n)
{
    int c = 0, ch;
    while (n--) {
	ch = tolower(ar[n]);
	if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')
	    c++;
    }

    return c;
}

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int rank, size, n, *res_buf, l, il;
    char sbuf[80], *rbuf;
    int i, j;
    int s = 0;

    MPI_Status status;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    res_buf = (int *) malloc(sizeof(int) * size);

    if (!rank) {
	fgets(sbuf, 79, stdin);
	sbuf[strlen(sbuf)-1] = '\0';
	l = strlen(sbuf);
	il = l / size;
    }

    MPI_Bcast(&il, 1, MPI_INT, 0, MPI_COMM_WORLD);
    rbuf = (char *) malloc(sizeof(char) * il);

    MPI_Scatter(sbuf, il, MPI_CHAR, rbuf, il, MPI_CHAR, 0, MPI_COMM_WORLD);

    n = il - nov(rbuf, il);

    MPI_Gather(&n, 1, MPI_INT, res_buf, 1, MPI_INT, 0, MPI_COMM_WORLD);

    if (!rank) {
	for (i = 0; i < size; i++)
	    s += res_buf[i];

	printf("%d\n", s);
    }

    MPI_Finalize();

    return 0;
}

