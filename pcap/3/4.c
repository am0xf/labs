#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"
#include <ctype.h>

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int rank, size, n, l, il, i;
    char sbuf1[80], sbuf2[80], *rbuf, *r2buf, *res_buf;

    MPI_Status status;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (!rank) {
	fgets(sbuf1, 79, stdin);
	fgets(sbuf2, 79, stdin);
	sbuf1[strlen(sbuf1)-1] = '\0';
	sbuf2[strlen(sbuf2)-1] = '\0';
	l = strlen(sbuf1);
	il = l / size;
	res_buf = (char *) malloc(sizeof(char) * (2*l + 2));
    }

    MPI_Bcast(&il, 1, MPI_INT, 0, MPI_COMM_WORLD);
    rbuf = (char *) malloc(2*il);

    MPI_Scatter(sbuf1, il, MPI_CHAR, &rbuf[0], il, MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Scatter(sbuf2, il, MPI_CHAR, &rbuf[il], il, MPI_CHAR, 0, MPI_COMM_WORLD);

    int t;
    for (i = 1; i < 2*il-1; i+=2) {
	t = rbuf[il+i/2];
	for (j = il + i/2; j > i; j--)
		rbuf[j] = rbuf[j-1];
	rbuf[i] = t;
    }

    MPI_Gather(rbuf, 2*il, MPI_CHAR, res_buf, 2*il, MPI_CHAR, 0, MPI_COMM_WORLD);

    if (!rank) {
	res_buf[2*l] = '\n';
	res_buf[2*l+1] = '\0';
	fputs(res_buf, stdout);
    }

    MPI_Finalize();

    return 0;
}

