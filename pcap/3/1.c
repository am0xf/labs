#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <ctype.h>

int fact(int n)
{
    return (n == 1)? 1 : n * fact(n-1);
}

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int rank, size, n;
    int *buf;
    int i;
    int s = 0;

    MPI_Status status;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    buf = (int *) malloc(sizeof(int) * size);

    if (!rank) {
	for (i = 0; i < size; i++)
	    buf[i] = i + 1;
    }

    MPI_Scatter(buf, 1, MPI_INT, &n, 1, MPI_INT, 0, MPI_COMM_WORLD);

    n = fact(n);

    MPI_Gather(&n, 1, MPI_INT, buf, 1, MPI_INT, 0, MPI_COMM_WORLD);

    if (!rank) {
	for (i = 0; i < size; i++)
	    s += buf[i];

	printf("%d\n", s);
    }

    MPI_Finalize();

    return 0;
}

