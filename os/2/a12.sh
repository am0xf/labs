#!/bin/bash

count() {
    c=0
    for fl in $(ls $1)
    do
	if [[ -d $1/$fl ]]
	then
	    t=$(count $1/$fl)
	    c=$[ $c + $t + 1 ]
	fi
    done

    echo $c
}

res=$(count $1)
echo $res

