#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

int sum(int n)
{
    return n + (n? sum(n-1) : n);
}

void* th_code(void *param)
{
    return (void *) sum((int) param);
}

int main(void)
{
    pthread_t ts;
    int rets;
    int n;

    pthread_create(&ts, NULL, &th_code, (void *) 5);

    pthread_join(ts, (void **) &rets);
    printf("%d\n", rets);

    return 0;
}

