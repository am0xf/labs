#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

int fib(int n)
{
    if (n == 1)
	return 0;
    if (n == 2)
	return 1;

    return fib(n-1) + fib(n-2);
}

void* th_code(void *param)
{
    return (void *) fib((int) param);
}

int main(void)
{
    pthread_t *ts;
    int *rets;
    int n = 10;

    ts = (pthread_t *) malloc(sizeof(pthread_t) * n);
    rets = (int *) malloc(sizeof(int) * n);

    int i;
    for(i = 0; i < n; i++) {
	pthread_create(&ts[i], NULL, &th_code, (void *) (i+1));
    }

    for(i = 0; i < n; i++) {
	pthread_join(ts[i], (void **) &rets[i]);
    }

    for(i = 0; i < n; i++) {
	printf("%d\n", rets[i]);
    }

    return 0;
}

