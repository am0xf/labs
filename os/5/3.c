#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

int prime_check(int n)
{
    int i;

    for(i = 2; i <= n / 2; i++)
	if (!(n%i))
	    return 0;
    return 1;
}

void* th_code(void *param)
{
    char *args = (char *) param;
    int from = (int) *(args + 0);
    int to = (int) *(args + 4);

    int *ar = (int *) malloc(sizeof(int) * (to - from + 1 + 1));
    int k = 1;

    while (to - from + 1) {
	if (prime_check(from))
	    ar[k++] = from;
	from++;
    }

    ar[0] = k - 1;

    return (void *) ar;
}

int main(void)
{
    pthread_t ts;
    int *rets;
    int from = 1, to = 99;

    int *args = (int *) malloc(sizeof(int) * 2);
    *args = from;
    *(args + 1) = to;
    
    pthread_create(&ts, NULL, &th_code, (void *) args);

    pthread_join(ts, (void **) &rets);

    int k = 1;
    while (k <= rets[0]) {
	printf("%d\n", rets[k++]);
    }

    return 0;
}

