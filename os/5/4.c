#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

void* th_code(void *param)
{
    char *args = (char *) param;
    int e_or_o = (int) *(args);
    int *ar = *((int **) (args + sizeof(int)));
    int n = (int) *(args + sizeof(int) + sizeof(int *));

    int s = 0;

    int i;
    for(i = 0; i < n; i++)
	if (ar[i] % 2 == e_or_o)
	    s += ar[i];

    return (void *) s;
}

int main(void)
{
    pthread_t t, s;
    int *rets = (int *) malloc(sizeof(int) * 2);

    char *args = (char *) malloc(sizeof(int) * 2 + sizeof(int*));
    char *args2 = (char *) malloc(sizeof(int) * 2 + sizeof(int*));

    int *ar = (int *) malloc(sizeof(int) * 10);
    int i;
    for(i = 0; i < 10; i++)
	ar[i] = i;

    *((int *) args) = 1;
    *((int **) (args + sizeof(int))) = ar;
    *((int *) (args + sizeof(int) + sizeof(int*))) = 10;

    pthread_create(&t, NULL, &th_code, (void *) args);
    
    *((int *) args2) = 0;
    *((int **) (args2 + sizeof(int))) = ar;
    *((int *) (args2 + sizeof(int) + sizeof(int*))) = 10;

    pthread_create(&s, NULL, &th_code, (void *) args2);

    pthread_join(t, (void **) &rets[0]);
    pthread_join(s, (void **) &rets[1]);

    printf("%d\n", rets[0]);
    printf("%d\n", rets[1]);

    return 0;
}

