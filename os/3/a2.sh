#!/bin/bash

declare -a arr
i=0

while true
do
    read -p "Enter number (-1 to exit) : " t

    [[ $t == -1 ]] && break || arr[$i]=$t
    i=$(( $i + 1 ))
done

sum=0

while [[ $i -gt 0 ]]
do
    sum=$(( $sum + ${arr[$(( $i - 1 ))]} ))
    i=$(( $i - 1 ))
done

echo $sum

