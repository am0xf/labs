#!/bin/bash

l=$(( ${#1} ))
l2=$(( ${#1} / 2 ))

for (( i=0; i < $l2; i++ ))
do
    if [[ ${1:$i:1} != ${1:$(($l-$i-1)):1} ]]
    then
	echo Not Palindrome
	exit 0
    fi
done

echo Palindrome
