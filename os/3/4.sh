#!/bin/bash

fact()
{
    [[ $1 == 1 ]] && echo $1 || echo $(( $1 * $( fact $(($1 - 1)) ) ))
}

fact $1

