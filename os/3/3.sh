#!/bin/bash

a=$1
b=$2
c=$3

d=$(( $b * $b - 4 * $a * $c ))

if [[ $d > 0 ]]
then
    echo "scale=5; (-1 * $b + sqrt($d)) / (2 * $a)" | bc
    echo "scale=5; (-1 * $b - sqrt($d)) / (2 * $a)" | bc

elif [[ $d == 0 ]]
then
    echo "scale=5; (-1 * $b / (2 * $a)" | bc

else
    echo Imaginary
fi

