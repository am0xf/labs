#include <stdlib.h>
#include <stdio.h>

void showmat(int **mat, int r, int c)
{
    printf("\n");
    int i, j;
    for(i = 0; i < r; i++) {
	for(j = 0; j < c; j++)
	    printf("%d ", mat[i][j]);

	printf("\n");
    }
}

int arrcmp(int *a, int *b, int n)
{
	int i;
    for (i = 0; i < n; i++) {
	if (a[i] < b[i]) 
	    return -1;
	else if (a[i] > b[i])
	    return 1;
	}
    return 0;
}

void arrop(int *a, int *b, int n, int op)
{
    for(n--; n != -1; n--)
	a[n] += op * b[n];
}

int safe(int *ar, int n)
{
	while (n && ar[n-1])
		n--;

    return !n;
}

int safestate(int *avail, int **alloc, int **need, int n, int m)
{
    int *work = (int *) calloc(sizeof(int), m);
    int *fin = (int *) calloc(sizeof(int), n);

    int i, c = 0;

	for(i = 0; i < m; i++)
		work[i] = avail[i];

    while (1) {
	for(i = 0; i < n; i++)
	    if (!fin[i] && arrcmp(need[i], work, m) <= 0)
		break;

	if (i == n)
	    break;

	arrop(work, alloc[i], n, 1);
	fin[i] = 1;
    }

    if (safe(fin, n)) {
	printf("Safe\n");
	return 1;
    }
	
    printf("Not Safe\n");
    return 0;
}

int main(void)
{
    FILE *f = fopen("2i", "r");

    int m, n;
    fscanf(f, "%d %d", &m, &n);

    int *avail = (int *) malloc(sizeof(int) * m);
    int **alloc = (int **) malloc(sizeof(int*) * n);
    int **need = (int **) malloc(sizeof(int*) * n);

    int i, j;

    // alloc input
    for(i = 0; i < n; i++) {
	alloc[i] = (int *) malloc(sizeof(int) * m);
	for(j = 0; j < m; j++)
	    fscanf(f, "%d", &alloc[i][j]);
    }

    // req input
    for(i = 0; i < n; i++) {
	need[i] = (int *) malloc(sizeof(int) * m);
	for(j = 0; j < m; j++) 
	    fscanf(f, "%d", &need[i][j]);
    }

    // avail input
    for(i = 0; i < m; i++)
	fscanf(f, "%d", &avail[i]);

    // a)
    safestate(avail, alloc, need, n, m);

	// b)
	need[2][2]++;
    safestate(avail, alloc, need, n, m);

    return 0;
}
