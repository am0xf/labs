#include <stdlib.h>
#include <stdio.h>

void showmat(int **mat, int r, int c)
{
    printf("\n");
    int i, j;
    for(i = 0; i < r; i++) {
	for(j = 0; j < c; j++)
	    printf("%d ", mat[i][j]);

	printf("\n");
    }
}

int arrcmp(int *a, int *b, int n)
{
	int i;
    for (i = 0; i < n; i++) {
	if (a[i] < b[i]) 
	    return -1;
	else if (a[i] > b[i])
	    return 1;
	}
    return 0;
}

void arrop(int *a, int *b, int n, int op)
{
    for(n--; n != -1; n--)
	a[n] += op * b[n];
}

int safe(int *ar, int n)
{
	while (n && ar[n-1])
		n--;

    return !n;
}

int safestate(int *avail, int **alloc, int **need, int n, int m)
{
    int *work = (int *) calloc(sizeof(int), m);
    int *fin = (int *) calloc(sizeof(int), n);
    int *ord = (int *) calloc(sizeof(int), n);

    int i, c = 0;

	for(i = 0; i < m; i++)
		work[i] = avail[i];

    while (1) {
	for(i = 0; i < n; i++)
	    if (!fin[i] && arrcmp(need[i], work, m) <= 0)
		break;

	if (i == n)
	    break;

	arrop(work, alloc[i], n, 1);
	fin[i] = 1;
	ord[c++] = i;
    }

    if (safe(fin, n)) {
	printf("Safe\nOrder : ");
	for(i = 0; i < n; i++)
	    printf("%d ", ord[i]);
	printf("\n");
	return 1;
    }
	
    printf("Not Safe\n");
    return 0;
}

void request(int pid, int *req, int **need, int *avail, int **alloc, int n, int m)
{
    int i;
    printf("\n%d : ", pid);
    for(i = 0; i < m; i++)
	printf("%d ", req[i]);
    printf("\n");

    if (arrcmp(req, need[pid], m) > 0) {
	printf("exceeded max\n");
	return;
    }

    if (arrcmp(req, avail, m) > 0) {
	printf("must wait\n");
	return;
    }

    arrop(avail, req, m, -1);
    arrop(alloc[i], req, m, 1);
    arrop(need[i], req, m, -1);

    if (safestate(avail, alloc, need, n, m))
	printf("Allocation possible\n");
    else
	printf("Allocation not possible\n");

    // only for question purposes this is common to both cases or else only if not possible
    arrop(avail, req, m, 1);
    arrop(alloc[i], req, m, -1);
    arrop(need[i], req, m, 1);
}

int main(void)
{
    FILE *f = fopen("1i", "r");

    int m, n;
    fscanf(f, "%d %d", &m, &n);

    int *avail = (int *) malloc(sizeof(int) * m);
    int *req = (int *) malloc(sizeof(int) * m);
    int **alloc = (int **) malloc(sizeof(int*) * n);
    int **need = (int **) malloc(sizeof(int*) * n);
    int **max = (int **) malloc(sizeof(int*) * n);

    int i, j;

    // alloc input
    for(i = 0; i < n; i++) {
	alloc[i] = (int *) malloc(sizeof(int) * m);
	for(j = 0; j < m; j++)
	    fscanf(f, "%d", &alloc[i][j]);
    }

    // max input
    for(i = 0; i < n; i++) {
	max[i] = (int *) malloc(sizeof(int) * m);
	need[i] = (int *) malloc(sizeof(int) * m);
	for(j = 0; j < m; j++) {
	    fscanf(f, "%d", &max[i][j]);
	    need[i][j] = max[i][j] - alloc[i][j];
	}
    }

    // avail input
    for(i = 0; i < m; i++)
	fscanf(f, "%d", &avail[i]);

    // a)
    showmat(max, n, m);

    // b)
    safestate(avail, alloc, need, n, m);

    // c)
    for(i = 0; i < m; i++)
	fscanf(f, "%d", &req[i]);

    request(1, req, need, avail, alloc, n, m);

    // d)
    for(i = 0; i < m; i++)
	fscanf(f, "%d", &req[i]);

    request(4, req, need, avail, alloc, n, m);

    // e)
    for(i = 0; i < m; i++)
	fscanf(f, "%d", &req[i]);

    request(0, req, need, avail, alloc, n, m);

    return 0;
}
