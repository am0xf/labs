#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include <sys/wait.h>

int main(void)
{
    int pid = fork();
    printf("PID : %d\n", getpid());
    printf("PPID : %d\n", getppid());
    printf("CPID : %d\n", pid);
    wait(NULL);
    
    exit(0);
}

