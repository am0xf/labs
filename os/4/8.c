#include <fcntl.h>

int main(void)
{
    int f = open("a", O_RDONLY); 

    char buf[80];

    int r = 0;
    int w, l, c;
    w = l = c = 0;

    while (1) {
	r = read(f, buf, 80);

	int i;

	c += r;

	for(i = 0; i < r; i++) {
	    if (buf[i] == '\n') {
		l++;
		w++;
	    }
	    else if (buf[i] == ' ')
		w++;
	}

	if (r < 80)
	    break;
    }

    close(f);

    printf("Characters : %d\n", c);
    printf("Words : %d\n", w + 1);
    printf("Line : %d\n", l);
    return 0;
}

