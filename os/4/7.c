#include <dirent.h>

#include <stdio.h>

int main(void)
{
    DIR *p = opendir(".");

    struct dirent *d;

    while (d = readdir(p)) {
	printf("%s\n", d->d_name);
    }

    closedir(p);

    return 0;
}

