#include <fcntl.h>

int main(void)
{
    int f = creat("a", S_IRUSR | S_IWUSR);

    char buf[] = {0xde, 0xad, 0xbe, 0xef};

    write(f, buf, 4);

    close(f);

    return 0;
}

