#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include <sys/wait.h>

int main(void)
{
    if (fork()) {
	printf("Parent2\n");
	wait(NULL);
    }
    else {
	execl("/home/170905170/os/l4/1", "1", NULL);
    }
    
    exit(0);
}

