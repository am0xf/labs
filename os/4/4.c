#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include <sys/wait.h>

int main(void)
{
    if (fork()) {
	printf("Parent\n");
    }
    else {
	printf("Child\n");
    }
    
    exit(0);
}

