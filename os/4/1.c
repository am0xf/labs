#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include <sys/wait.h>

void do_child(void)
{
    printf("Child\n");
}

int main(void)
{
    printf("Parent\n");

    if (fork()) {
	printf("Parent waiting\n");
	wait(NULL);
	printf("Parent waiting finished\n");
	exit(0);
    }
    else {
	do_child();
	exit(0);
    }
}

