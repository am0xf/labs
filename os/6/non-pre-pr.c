#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int tid, wait, ta, arr, burst, priority;
} Task;

Task *newTask(int id, int arr, int burst, int p)
{
    Task *t = (Task *) malloc(sizeof(Task));

    t->tid = id;
    t->arr = arr;
    t->burst = burst;
    t->wait = t->ta = 0;
    t->priority = p;

    return t;
}

int main(void)
{
    Task *exq[4];
    int xqp = 0;
    int cur_x = 0;

    int inp[4][3];
    inp[0][0] = 0;
    inp[0][1] = 60;
    inp[0][2] = 3;
    inp[1][0] = 3;
    inp[1][1] = 30;
    inp[1][2] = 2;
    inp[2][0] = 4;
    inp[2][1] = 40;
    inp[2][2] = 1;
    inp[3][0] = 9;
    inp[3][1] = 10;
    inp[3][2] = 4;
    int inp_p = 0;

    int tick = 0;
    int i, next_high_pr; // low value of priority means higher priority

    while (1) {
	// add new tasks
	while (inp[inp_p][0] == tick) {
	    exq[xqp++] = newTask(inp_p, tick, inp[inp_p][1], inp[inp_p][2]);
	    inp_p++;
	}

	// check switch
	if (exq[cur_x]->burst == 0) {
	    exq[cur_x]->ta = tick - exq[cur_x]->arr;

	    next_high_pr = cur_x;
	    for(i = 0; i < xqp; i++) {
		if (exq[next_high_pr]->burst == 0 || exq[i]->burst != 0 && exq[i]->priority < exq[next_high_pr]->priority)
		    next_high_pr = i;
	    }

	    if (exq[next_high_pr]->burst == 0)
		break;

	    cur_x = next_high_pr;
	}

	//printf("Time : %d Task : %d Pr : %d\n", tick, cur_x, exq[cur_x]->priority);

	// adj times
	for(i = 0; i < xqp; i++) {
	    if (cur_x == i)
		exq[i]->burst--;
	    else if (exq[i]->burst)
		exq[i]->wait++;
	}

	tick++;

    }
    printf("\n\n");
    float av_w = 0, av_ta = 0;
    for(i = 0; i < 4; i++) {
	printf("Task : %d Waiting : %d TA : %d\n", i, exq[i]->wait, exq[i]->ta);
	av_w += exq[i]->wait;	
	av_ta += exq[i]->ta;	
    }

    printf("\n");
    printf("Avg W : %f\n", av_w / 4);
    printf("Avg TA : %f\n", av_ta / 4);

    return 0;
}

