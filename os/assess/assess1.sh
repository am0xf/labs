#!/bin/bash

read -p "Enter a number : " num

sod=0

while [[ $num -ne 0 ]]
do
    sod=$[ $sod + $num % 10 ]
    num=$[ $num / 10 ]
done

flag=0

if [[ $[$sod % 2] ==  0 ]]
then
    flag=1
else
    flag=2
fi

case $flag in
    "1")
	echo Even
	;;
    "2")
	echo Odd
	;;
esac

