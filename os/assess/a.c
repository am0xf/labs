#include <math.h>
#include <stdio.h>

struct ent {
    int base, limit, alloc;
};

int rnd(int a)
{
    int i;
    for (i = 0; ; i++)
	if ( (int) pow(2, i) >= a)
	    return (int) pow(2, i);
}

int check(int base, int alloc, struct ent t[4])
{
    int i;
    for (i = 0; i < 4; i++) {
	if (base >= t[i].base && base < t[i].base + t[i].alloc)
	    return 1;
	if (base + alloc - 1 >= t[i].base && base + alloc - 1 < t[i].base + t[i].alloc)
	    return 1;
    }
    return 0;
}

int main(void)
{
    struct ent tab[4];

    int i;
    for (i = 0; i < 4; i++)
	tab[i].base = tab[i].limit = tab[i].alloc = -1;

    int n, r;
    printf("No. of table entries : ");
    scanf("%d", &n);

    int sn, bs, lm, allc;
    for (i = 0; i < n; i++) {
	printf("Segment no. : ");
	scanf("%d", &sn);
	printf("Base Limit : ");
	scanf("%d %d", &bs, &lm);
	allc = rnd(lm);

	r = check(bs, allc, tab);

	if (r == 0) {
	    tab[i].base = bs;
	    tab[i].limit = lm;
	    tab[i].alloc = allc;
	}
	else {
	    printf("Overlapping Segments\n");
	    i--;
	}
    }

    i = 0;
    while (i < 4) {
	printf("Segment no. Logical address : ");
	scanf("%d %d", &sn, &lm);

	if (tab[sn].base == -1) {
	    printf("Segment not in table\n");
	    continue;
	}

	printf("Physical Address : %d\n", tab[sn].base + lm);
	printf("Page number : %d\n", lm / 512);

	if (lm >= tab[sn].limit)
	    printf("Illegal Address\n");
	else
	    printf("Legal Address\n");

	i++;
    }

    return 0;
}


