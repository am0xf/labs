#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *sum(void *args)
{
    int *ar = *((int**) args);
    int n = *((int*)(args+sizeof(int*)));

    int s = 0;
    int i;
    for (i = 0; i < n; i++)
	s += ar[i];

    return (void *) s;
}

int main(void)
{
    pthread_t fh, sh;
    int *rs = (int *) malloc(sizeof(int) * 2);

    int n, *ar;
    scanf("%d", &n);
    ar = (int *) malloc(sizeof(int) * n);
    int i;
    for(i = 0; i < n; i++)
	scanf("%d", &ar[i]);

    void *args1 = malloc(sizeof(int) + sizeof(int*));
    void *args2 = malloc(sizeof(int) + sizeof(int*));

    *((int**) args1) = ar;
    *((int**) args2) = ar + n / 2;

    *((int*) (args1 + sizeof(int*))) = n / 2;
    *((int*) (args2 + sizeof(int*))) = n - n / 2;

    pthread_create(&fh, 0, sum, args1);
    pthread_create(&sh, 0, sum, args2);

    pthread_join(fh, (void **) &rs[0]);
    pthread_join(sh, (void **) &rs[1]);

    printf("1st half : %d\n", rs[0]);
    printf("2nd half : %d\n", rs[1]);
    printf("Sum : %d\n", rs[0] + rs[1]);
    
    return 0;
}

