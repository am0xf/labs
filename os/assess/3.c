#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int in(int *ar, int n, int c)
{
    int i;
    for (i = 0; i < n; i++)
	if (ar[i] == c)
	    return i;

   return -1;
}

int main(void)
{
    int n;
    scanf("%d", &n);

    int *rqs = (int *) malloc(sizeof(int) * n);

    int i;
    for (i = 0; i < n; i++)
	scanf("%d", rqs + i);

    int cs, dir = -1, mxc = 200 - 1;
    scanf("%d", &cs);

    int rs = 0;
    int totm = 0, d = 0;
    int nxi;

    while (1) {
	nxi = in(rqs, n, cs);
	if (nxi != -1) {
	    rqs[nxi] = -1;
	    rs++;
	    if (rs == n)
		break;
	}
	if (cs == mxc || cs == 0)
	    dir *= -1;
	cs += dir;
	totm++;
    }

    printf("Total Movement : %d\n", totm);
		
    return 0;
}

