#!/bin/bash

# 1
mkdir -p a/{b,c}

# 2
touch a/b/{1,2,3}
echo abcd > a/b/1
echo efgh > a/b/2
echo ijkl > a/b/3

# 3
cp a/{b/1,c}

# 4
cat > a/c/2 << EOF
who
ls
EOF

# 5
ls {a,A}*

# 6
outp=$(gcc -o short short.c)
if [ $? -eq 0 ]
then
    cat short.c
else
    echo Error
fi

# 7
echo 7
read -p "Enter a file name : " name
head -n 5 $name
tail -n 5 $name

# 8
pwd > 8
date >> 8
ls >> 8

# a1
echo a1
read -p "Enter a file name : " name
sort -t, -k 2 $name

# a2
grep -c 50 -r .

# a3
echo a3
read -p "Enter a file name : " name
grep -m 2 OS $name

