#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int n;
    scanf("%d", &n);

    int *rqs = (int *) malloc(sizeof(int) * n);

    int i;
    for (i = 0; i < n; i++)
	scanf("%d", rqs + i);

    int cs;
    scanf("%d", &cs);

    int rs = 0;
    int totm = 0, d = 0;
    int nx;

    for (; rs < n; rs++) {
	nx = rqs[rs];
	d = cs - nx;
	totm += d < 0 ? -d : d;
	cs = nx;
    }

    printf("Total Movement : %d\n", totm);
		
    return 0;
}

