#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int find(int *ar, int n, int c, int d)
{
    int i;
    for (i = 0; i < n; i++) {
	if (d == 1) {
	    if (ar[i] > c)
		return i;
	}
	else {
	    if (ar[i] != -1 && ar[i] < c)
		return i;
	}
    }
   return -1;
}

int main(void)
{
    int n;
    scanf("%d", &n);

    int *rqs = (int *) malloc(sizeof(int) * n);

    int i;
    for (i = 0; i < n; i++)
	scanf("%d", rqs + i);

    int cs, dir = -1, mxc = 200 - 1;
    scanf("%d", &cs);

    int rs = 0;
    int totm = 0, d = 0;
    int nxi, nx;

    while (1) {
	nxi = find(rqs, n, cs, dir);

	if (nxi != -1) {
	    nx = rqs[nxi];
	    d = cs - nx;
	    d = d < 0 ? -d : d;
	    totm += d;
	    cs = nx;
	    rqs[nxi] = -1;
	    rs++;
	    if (rs == n)
		break;
	}
	else
	    dir *= -1;
    }

    printf("Total Movement : %d\n", totm);
		
    return 0;
}

