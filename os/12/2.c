#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int abs_diff(int a, int b)
{
    int d = a - b;
    return d < 0 ? -d : d;
}

int sst(int *ar, int n, int c)
{
    int i;
    int sk = INT_MAX, ski = -1;
    for (i = 0; i < n; i++)
	if (ar[i] != -1)
	    if (abs_diff(ar[i], c) < sk) {
		sk = abs_diff(ar[i], c);
		ski = i;
	    }
   return ski;
}

int main(void)
{
    int n;
    scanf("%d", &n);

    int *rqs = (int *) malloc(sizeof(int) * n);

    int i;
    for (i = 0; i < n; i++)
	scanf("%d", rqs + i);

    int cs;
    scanf("%d", &cs);

    int rs = 0;
    int totm = 0, d = 0;
    int nx, nxi;

    for (; rs < n; rs++) {
	nxi = sst(rqs, n, cs);
	nx = rqs[nxi];
	rqs[nxi] = -1;
	d = cs - nx;
	totm += d < 0 ? -d : d;
	cs = nx;
    }

    printf("Total Movement : %d\n", totm);
		
    return 0;
}

