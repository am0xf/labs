#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
    int q[3][2];

    char *inp = (char *) malloc(sizeof(char) * 80);

    fgets(inp, 79, stdin);

    int l = strlen(inp);
    inp[l-1] = '\0';
    l--;

    int i = 0, qp = 0, req;
    int pf = 0;
    while (qp < 3 && i < l) {
	req = inp[i++];
	if (q[0][0] != req && q[1][0] != req && q[2][0] != req) {
	    q[qp++][0] = req; 
	    pf++;
	    printf("%d %d %d\n", q[0][0] - 48, q[1][0] - 48, q[2][0] - 48);
	}
    }

    int j, k;
    while (i < l) {
       req = inp[i++];

	if (q[0][0] != req && q[1][0] != req && q[2][0] != req) {
	    for (j = 0; j < 3; j++) {
		for (k = i; k < l && inp[k] != q[j][0]; k++);
		q[j][1] = k;
	    }

	    j = q[0][1] < q[1][1] ? 1 : 0;
	    j = q[j][1] < q[2][1] ? 2 : j;

	    for (; j < 2; j++)
		q[j][0] = q[j+1][0];

	    q[2][0] = req;
	    printf("%d %d %d\n", q[0][0] - 48, q[1][0] - 48, q[2][0] - 48);
	    pf++;
	}
    }	

    printf("%d\n", pf);

    return 0;
}

