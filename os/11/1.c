#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
    int q[3];

    char *inp = (char *) malloc(sizeof(char) * 80);

    fgets(inp, 79, stdin);

    int l = strlen(inp);
    inp[l-1] = '\0';
    l--;

    int pf = 0;
    int i = 0, qp = 0;
    char req;
    while (qp < 3 && i < l) {
	req = inp[i++];
	if (q[0] != req && q[1] != req && q[2] != req) {
	    pf++;
	    q[qp++] = req; 
	    printf("%c %c %c\n", q[0], q[1], q[2]);
	}
    }

    while (i < l) {
       req = inp[i++];

	if (q[0] != req && q[1] != req && q[2] != req) {
	    pf++;
	    q[0] = q[1];
	    q[1] = q[2];
	    q[2] = req;
	    printf("%c %c %c\n", q[0], q[1], q[2]);
	}
    }	

    printf("%d\n", pf);
    return 0;
}

