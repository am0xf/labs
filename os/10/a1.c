#include <stdio.h>
#include <stdlib.h>

struct descriptor {
    long start, limit;
};

int main(void)
{
    struct descriptor table[5];

    int i, t, n;
    for (i = 0; i < 5; i++) {
	printf("Enter segment number : ");
	scanf("%d", &t);
	printf("Enter segment starting address : ");
	scanf("%lx", &table[t].start);
	printf("Enter segment limit : ");
	scanf("%lx", &table[t].limit);
    }

    for (i = 0; i < 3; i++) {
	printf("Enter logical address : ");
	scanf("%d, %d", &n, &t);

	if (table[t].limit < n)
	    printf("Segmentation Fault. Byte not in this segment\n");
	else
	    printf("Physic address : %ld, 0x%lx\n", table[t].start + n, table[t].start + n);
    }

    return 0;
}

