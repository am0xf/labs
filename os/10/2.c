#include <stdio.h>
#include <stdlib.h>

struct logaddr {
    unsigned int offset : 5;
    unsigned int index : 3;
};

int main(void)
{
    struct logaddr la;
    short inp;

    int i;
    for(i = 0; i < 2; i++) {
        printf("Enter a address : ");
	scanf("%hd", &inp);

        la = *((struct logaddr *) &inp);

        printf("Index : %d\nOffset : %d\n", la.index, la.offset);
    }

    return 0;
}

