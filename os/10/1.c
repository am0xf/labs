#include <stdio.h>
#include <stdlib.h>

struct partition {
    int size, used; // in KBs
};

int main(void)
{
    struct partition table[5];

    table[0].size = 100;
    table[1].size = 500;
    table[2].size = 200;
    table[3].size = 300;
    table[4].size = 600;

    int i, j;
    for (i = 0; i < 5; i++)
	table[i].used = 0;

    int inp[5];

    double s;
    int tot = 1700;

    for (i = 0; i < 4; i++)
	scanf("%d", &inp[i]);

    // first-fit

    printf("First Fit\n");
    s = 0;
    for (i = 0; i < 4; i++) {
	for (j = 0; j < 5; j++) {
	    if (table[j].size - table[j].used >= inp[i])
		break;		    
	}

	if (j == 5)
	    printf("Can't fit %d\n", inp[i]);
	else {
	    table[j].used += inp[i];
	    s += inp[i];
	    printf("%d : %d\n", table[j].size, table[j].size - table[j].used);
	}
    }

    for (i = 0; i < 5; i++)
	table[i].used = 0;

    printf("%% used : %.2f\n", s * 100 / tot);

    // best-fit

    printf("Best Fit\n");
    s = 0;
    int b = -1;
    for (i = 0; i < 4; i++) {
	b = -1;
	for (j = 0; j < 5; j++)
	    if (table[j].size - table[j].used >= inp[i]) {
		if (b == -1)
		    b = j;
		else if (table[j].size - table[j].used - inp[i] < table[b].size - table[b].used - inp[i])
		    b = j;
	    }

	if (b == -1)
	    printf("Can't fit %d\n", inp[i]);
	else {
	    table[b].used += inp[i];
	    s += inp[i];
	    printf("%d : %d\n", table[b].size, table[b].size - table[b].used);
	}
    }

    for (i = 0; i < 5; i++)
	table[i].used = 0;

    printf("%% used : %.2f\n", s * 100 / tot);

    // worst-fit

    printf("Worst Fit\n");
    s = 0;
    b = -1;
    for (i = 0; i < 4; i++) {
	b = -1;
	for (j = 0; j < 5; j++)
	    if (table[j].size - table[j].used >= inp[i]) {
		if (b == -1)
		    b = j;
		else if (table[j].size - table[j].used - inp[i] > table[b].size - table[b].used - inp[i])
		    b = j;
	    }

	if (b == -1)
	    printf("Can't fit %d\n", inp[i]);
	else {
	    table[b].used += inp[i];
	    s += inp[i];
	    printf("%d : %d\n", table[b].size, table[b].size - table[b].used);
	}
    }

    for (i = 0; i < 5; i++)
	table[i].used = 0;

    printf("%% used : %.2f\n", s * 100 / tot);

    return 0;
}

