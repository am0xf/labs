#include <pthread.h>
#include <semaphore.h>

#include <unistd.h>

#include <stdio.h>

sem_t mutex, writing;
int data = 0, readcount = 0;

void* reader(void *arg)
{
    do {
        sem_wait(&mutex);
	readcount++;

	if (readcount == 1)
	    sem_wait(&writing);

	sem_post(&mutex);

        printf("Read %d : %d\n", (int) arg, data);

	sem_wait(&mutex);
	readcount--;

	if (readcount == 0)
	    sem_post(&writing);

        sem_post(&mutex);

	sleep(1);
    } while (1);
}

void* writer(void *arg)
{
    int id = (int) arg;
    int i = id; 

    do {
	sem_wait(&writing);

	data = i;
        printf("Wrote %d : %d\n", id, data);
	i = i + 2;

        sem_post(&writing);
	
	sleep(1);
    } while (i < 15);
}

int main(void)
{
    sem_init(&mutex, 0, 1);
    sem_init(&writing, 0, 1);

    pthread_t w1, w2, r1, r2;
    pthread_create(&w1, NULL, writer, (void*) 1);
    pthread_create(&w2, NULL, writer, (void*) 2);
    pthread_create(&r1, NULL, reader, (void*) 1);
    pthread_create(&r2, NULL, reader, (void*) 2);

    pthread_join(w1, 0);
    pthread_join(w2, 0);
    pthread_join(r1, 0);
    pthread_join(r2, 0);

    return 0;
}
