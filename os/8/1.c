#include <pthread.h>
#include <semaphore.h>

#include <unistd.h>

#include <stdio.h>

#define BFSZ 10

sem_t mutex, space_left, data_present;
int buf[BFSZ], f = -1, r = -1;

void* consumer(void *arg)
{
    do {
	sem_wait(&data_present);

        sem_wait(&mutex);

        printf("C : %d\n", buf[(++f) % BFSZ]);

        sem_post(&mutex);

        sem_post(&space_left);
	
    } while (1);
}

void* producer(void *arg)
{
    int i = 1, v, id = (int) arg;

    do {
	sem_wait(&space_left);

        sem_wait(&mutex);

        printf("P%d : %d\n", id, (buf[(++r) % BFSZ] = i++));

        sem_post(&mutex);

	sem_getvalue(&space_left, &v);
	if (v == 0)
	    sem_post(&data_present);
	
	sleep(1);
    } while (i < 100);
}

int main(void)
{
    sem_init(&mutex, 0, 1);
    sem_init(&space_left, 0, BFSZ);
    sem_init(&data_present, 0, 0);

    pthread_t p1, p2, c;
    pthread_create(&p1, NULL, producer, (void*) 1);
    //pthread_create(&p2, NULL, producer, (void*) 2);
    pthread_create(&c, NULL, consumer, (void*) 0);

    pthread_join(p1, 0);
    pthread_join(p2, 0);
    pthread_join(c, 0);

    return 0;
}
