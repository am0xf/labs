#!/usr/bin/ns

set ns [new Simulator]

set nt [open 4.nam w]
$ns namtrace-all $nt

set tr [open 4.tr w]
$ns trace-all $tr

proc finish {} {
    global ns nt tr
    $ns flush-trace
    close $nt
    close $tr
    exec nam 4.nam &
    exit 0
}

set s [$ns node]
set r [$ns node]
set d [$ns node]

$ns duplex-link $s $r 1Mb 50ms DropTail
$ns duplex-link-op $r $s orient right

$ns duplex-link $r $d 100Kb 5ms DropTail
$ns duplex-link-op $r $d orient left 
$ns queue-limit $r $d 10
$ns duplex-link-op $r $d queuePos 1.5

set udpnda1 [new Agent/UDP]
$ns attach-agent $s $udpnda1

set cbrnda1 [new Application/Traffic/CBR]
$cbrnda1 attach-agent $udpnda1
$cbrnda1 set type_ CBR
$cbrnda1 set packet_size_ 1000
$cbrnda1 set rate_ 160kb
$cbrnda1 set random_ false 

set nullnra1 [new Agent/Null]
$ns attach-agent $d $nullnra1
$ns connect $udpnda1 $nullnra1


$ns at 0 "$cbrnda1 start"

$ns at 4 "$cbrnda1 stop"

$ns at 10 "finish"

$ns run

