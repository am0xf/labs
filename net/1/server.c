#include "headers.h"

int main(void)
{
    int sid = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in ad;
    struct sockaddr_in cad;

    ad.sin_family = AF_INET;
    ad.sin_addr.s_addr = inet_addr("127.0.0.1");
    ad.sin_port = htons(12345);
    
    int res = bind(sid, (struct sockaddr *) &ad, sizeof(ad));

    char buf[256];

    listen(sid, 2);
    while (1) {
	int cl = sizeof(cad);
        int newsid = accept(sid, (struct sockaddr *) &cad, &cl);
        int n = read(newsid, buf, sizeof(buf));

        printf("%s\n", buf);

	close(newsid);
    }

    close(sid);

    return 0;
}

