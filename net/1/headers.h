#include <sys/types.h>

#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdio.h>

#include <stdlib.h>

#include <string.h>

#define BUFLEN 1024
