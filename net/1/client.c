#include "headers.h" 

int main(void)
{
    int sid = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in ad;
    ad.sin_family = AF_INET;
    ad.sin_addr.s_addr = inet_addr("127.0.0.1");
    ad.sin_port = htons(12345);
    
    int res = connect(sid, (struct sockaddr *) &ad, sizeof(ad));

    char buf[80];
    scanf("%s", buf);
    buf[strlen(buf)] = '\0';
    write(sid, buf, strlen(buf));

    close(sid);

    return 0;
}

