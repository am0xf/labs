#include "headers.h" 

void createSocket(struct sockaddr_in *ad)
{
    ad->sin_family = AF_INET;
    ad->sin_addr.s_addr = inet_addr("127.0.0.1");
    ad->sin_port = htons(12345);
}

int main(void)
{
    int sid = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in *ad;
    ad = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));

    createSocket(ad);

    int res = connect(sid, (struct sockaddr *) ad, sizeof(struct sockaddr_in));

    char buf[BUFLEN];

    buf[0] = 'G';
    buf[1] = 'e';
    buf[2] = 't';
    buf[3] = '\0';
    buf[BUFLEN-1] = '\0';

    write(sid, buf, BUFLEN);

    read(sid, buf, BUFLEN);

    printf("%s", buf);

    close(sid);

    return 0;
}

