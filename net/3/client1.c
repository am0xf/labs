#include "headers.h" 

void createSocket(struct sockaddr_in *ad)
{
    ad->sin_family = AF_INET;
    ad->sin_addr.s_addr = inet_addr("127.0.0.1");
    ad->sin_port = htons(12345);
}

int main(void)
{
    int sid = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in *ad;
    ad = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));

    createSocket(ad);

    int res = connect(sid, (struct sockaddr *) ad, sizeof(struct sockaddr_in));

    int n = 5;
    int ar[] = {123, 324, 453,36 ,123};

    int tn = htonl(n);
    
    write(sid, &tn, sizeof(int));

    int i;
    for(i = 0; i < n; i++) {
	tn = htonl(ar[i]);
	write(sid, &tn, sizeof(int));
    }

    for(i = 0; i < n; i++) {
	read(sid, &tn, sizeof(int));
	ar[i] = ntohl(tn);
    }

    close(sid);

    for(i =  0; i < n; i++) {
	printf("%d\n", ar[i]);
    }

    return 0;
}

