#include "headers.h"

int sid;

void createSocket(struct sockaddr_in *ad)
{
    ad->sin_family = AF_INET;
    ad->sin_addr.s_addr = inet_addr("127.0.0.1");
    ad->sin_port = htons(12345);
} 

void close_grace(int signum)
{
    shutdown(sid, SHUT_RDWR);
    close(sid);
    exit(0);
}

int main(void)
{
    signal(SIGINT, close_grace);

    sid = socket(AF_INET, SOCK_DGRAM, 0);
    struct sockaddr_in *ad, cad;
    ad = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));

    createSocket(ad);
    int res = bind(sid, (struct sockaddr *) ad, sizeof(struct sockaddr_in));

    int recvd, cl;

    int tn;
    cl = sizeof(cad);
    recvd = recvfrom(sid, &tn, sizeof(int), 0, (struct sockaddr *) &cad, &cl);
    
    int n = ntohl(tn);

    int **ar = (int **) malloc(sizeof(int*) * n);

    int i, j;
    for(i = 0; i < n; i++) {
	ar[i] = (int *) malloc(sizeof(int) * n);

	for(j = 0; j < n; j++) {
	    recvd = recvfrom(sid, &tn, sizeof(int), 0, (struct sockaddr *) &cad, &cl);
	    ar[i][j] = ntohl(tn);
	}
    }

    for(i = 0; i < n; i++) {
	for(j = 0; j < n; j++)
	    printf("%d ", ar[i][j]);
	printf("\n");
    }

    close_grace(0);

    return 0;
}

