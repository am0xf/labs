#include "headers.h" 

void createSocket(struct sockaddr_in *ad)
{
    ad->sin_family = AF_INET;
    ad->sin_addr.s_addr = inet_addr("127.0.0.1");
    ad->sin_port = htons(12345);
}

int main(void)
{
    int sid = socket(AF_INET, SOCK_DGRAM, 0);
    struct sockaddr_in *ad;
    ad = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));

    createSocket(ad);

    int n = 5;
    int tn = htonl(n);
    int sent;
    sent = sendto(sid, &tn, sizeof(int), 0, (struct sockaddr *) ad, sizeof(struct sockaddr_in));
    int i, j;
    for(i = 0; i < n; i++)
	for(j = 0; j < n; j++) {
	    tn = htonl(i + j);
	    sent = sendto(sid, &tn, sizeof(int), 0, (struct sockaddr *) ad, sizeof(struct sockaddr_in));
	}

    close(sid);

    return 0;
}

