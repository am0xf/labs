#include "headers.h"

#include <time.h>

int sid;
fd_set master, test;

void createSocket(struct sockaddr_in *ad)
{
    ad->sin_family = AF_INET;
    ad->sin_addr.s_addr = inet_addr("127.0.0.1");
    ad->sin_port = htons(12345);
} 

void getdata(char *buf)
{
    time_t sec = time(NULL);
    struct tm *loc_tm = localtime(&sec);
   
    snprintf(buf, BUFLEN, "%d %s\n", getpid(), asctime(loc_tm)); 
}

void serve(int sock_id)
{
    char *buf = (char *) malloc(BUFLEN);
    int nread = read(sock_id,buf, BUFLEN);
    
    if (nread < 1) {
	close(sock_id);

	FD_CLR(sock_id, &master);

	printf("Client left\n");
	return;
    }

    getdata(buf);
	
    write(sock_id, buf, BUFLEN);
}

void close_grace(int signum)
{
    shutdown(sid, SHUT_RDWR);
    FD_ZERO(&master);
    FD_ZERO(&test);
    close(sid);
    exit(0);
}

int main(void)
{
    signal(SIGINT, close_grace);

    FD_ZERO(&master);

    sid = socket(AF_INET, SOCK_STREAM, 0);

    FD_SET(sid, &master);

    struct sockaddr_in *ad, cad;
    ad = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));

    createSocket(ad);
    int res = bind(sid, (struct sockaddr *) ad, sizeof(struct sockaddr_in));
    int cl, nsid;

    listen(sid, 2);

    while (1) {
	test = master;
	res = select(FD_SETSIZE, &test, NULL, NULL, NULL);

	if (res < 1)
	    exit(1);

	int i;
	for(i = 0; i < FD_SETSIZE; i++) {
	    if (FD_ISSET(i, &test)) {
		if (i == sid) {
		    cl = sizeof(cad);
	            nsid = accept(sid, (struct sockaddr *) &cad, &cl);
		    FD_SET(nsid, &master);
		    printf("New Client\n");
		}

		else {
		    serve(i);
		}
	    }
	}
    }

    return 0;
}

