#include "headers.h"

int sid;

void createSocket(struct sockaddr_in *ad)
{
    ad->sin_family = AF_INET;
    ad->sin_addr.s_addr = inet_addr("127.0.0.1");
    ad->sin_port = htons(12345);
} 

void sort(int *ar, int n)
{
    int t;
    int i, j;
    for(i = 0;i < n; i++) {
	for(j = i + 1; j < n; j++) {
	    if(ar[i] > ar[j]) {
		t = ar[i];
		ar[i] = ar[j];
		ar[j] = t;
	    }
	}
    }
}

void serve(int sock_id)
{
    int tn;
    read(sock_id, &tn, sizeof(int));

    int n = ntohl(tn);
    
    int *ar = (int *) malloc(sizeof(int) * n);

    int i;
    for(i = 0; i < n; i++) {
	read(sock_id, &tn, sizeof(int));
	ar[i] = ntohl(tn);
    }

    sort(ar, n);

    for(i = 0; i < n; i++) {
	tn = htonl(ar[i]);
	write(sock_id, &tn, sizeof(int));
    }
    close(sock_id);
}

void close_grace(int signum)
{
    shutdown(sid, SHUT_RDWR);
    close(sid);
    exit(0);
}

int main(void)
{
    signal(SIGINT, close_grace);

    sid = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in *ad, cad;
    ad = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));

    createSocket(ad);
    int res = bind(sid, (struct sockaddr *) ad, sizeof(struct sockaddr_in));

    int nsid, pid, cl;

    listen(sid, 2);

    while (1) {
	cl = sizeof(cad);
        nsid = accept(sid, (struct sockaddr *) &cad, &cl);
	
	pid = fork();

	if (!pid) {
	    close(sid);
	    serve(nsid);
	}
	close(nsid);
    }

    return 0;
}

