#!/usr/bin/ns

set ns [new Simulator]
set tr [open a.tr w]
set nm [open a.nam w]

$ns trace-all $tr
$ns namtrace-all $nm

proc finish {} {
    global tr nm ns
    $ns flush-trace

    close $tr
    close $nm

    exec nam a.nam &

    exit 0
}

set n0 [$ns node]
set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]
set n4 [$ns node]

$ns duplex-link $n0 $n2 1Mb 10ms DropTail
$ns duplex-link $n1 $n2 2Mb 10ms DropTail
$ns duplex-link $n2 $n3 2Mb 10ms DropTail
$ns duplex-link $n3 $n4 2Mb 10ms DropTail

$ns duplex-link-op $n2 $n0 orient left-up
$ns duplex-link-op $n2 $n1 orient left-down
$ns duplex-link-op $n2 $n3 orient right
$ns duplex-link-op $n3 $n4 orient right

$ns duplex-link-op $n2 $n3 queuePos 0.5
$ns duplex-link-op $n3 $n4 queuePos 0.5

$ns queue-limit $n2 $n3 5
$ns queue-limit $n3 $n4 5

set udp [new Agent/UDP]
$ns attach-agent $n0 $udp

set cbr [new Application/Traffic/CBR]
$cbr set type_ CBR
$cbr set packet_size_ 500
$cbr set rate_ 1mb
$cbr set random_ false
$cbr attach-agent $udp

set sink1 [new Agent/Null]
$ns attach-agent $n4 $sink1

$ns connect $udp $sink1

set tcp [new Agent/TCP]
$tcp set class_ 1
$ns attach-agent $n1 $tcp

set ftp [new Application/FTP]
$ftp attach-agent $tcp

set sink2 [new Agent/TCPSink]
$ns attach-agent $n4 $sink2

$ns connect $tcp $sink2

$ns at 0.5 "$cbr start"
$ns at 1 "$ftp start"
$ns at 3.5 "$cbr stop"
$ns at 3.5 "$ftp stop"

$ns at 4 "finish"

$ns run
