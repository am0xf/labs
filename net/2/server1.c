#include "headers.h"

#include <unistd.h>
#include <time.h>

#include <signal.h>

int sid;

void createSocket(struct sockaddr_in *ad)
{
    ad->sin_family = AF_INET;
    ad->sin_addr.s_addr = inet_addr("127.0.0.1");
    ad->sin_port = htons(12345);
} 

void getdata(char *buf)
{
    time_t sec = time(NULL);
    struct tm *loc_tm = localtime(&sec);
   
    snprintf(buf, BUFLEN, "%d %s\n", getpid(), asctime(loc_tm)); 
}

void close_grace(int signum)
{
    shutdown(sid, SHUT_RDWR);
    close(sid);
    exit(0);
}

int main(void)
{
    sid = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in *ad, cad;
    ad = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));

    createSocket(ad);
    int res = bind(sid, (struct sockaddr *) ad, sizeof(struct sockaddr_in));

    char *buf = (char *) malloc(BUFLEN);

    listen(sid, 2);

    while (1) {
	int cl = sizeof(cad);
        int nsid = accept(sid, (struct sockaddr *) &cad, &cl);
	
	signal(SIGINT, close_grace);

	getdata(buf);
	
	write(nsid, buf, BUFLEN);

	close(nsid);
    }

    return 0;
}

