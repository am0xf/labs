#include "headers.h"

#include <signal.h>

int sid;
struct sockaddr_in *ad, cad;

void createSocket(struct sockaddr_in *ad)
{
    ad->sin_family = AF_INET;
    ad->sin_addr.s_addr = inet_addr("127.0.0.1");
    ad->sin_port = htons(12345);
} 

void getdata(char *buf, char *retbuf)
{
    char addr[80];
    inet_ntop(AF_INET, &ad->sin_addr.s_addr, addr, 80);
    snprintf(retbuf, BUFLEN, "%s:%d %s\n", addr, ntohs(ad->sin_port), buf); 
}

void close_grace(int signum)
{
    shutdown(sid, SHUT_RDWR);
    close(sid);
    exit(0);
}

int main(void)
{
    sid = socket(AF_INET, SOCK_STREAM, 0);
    ad = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));

    createSocket(ad);
    int res = bind(sid, (struct sockaddr *) ad, sizeof(struct sockaddr_in));

    char *buf = (char *) malloc(BUFLEN);
    char *retbuf = (char *) malloc(BUFLEN);

    listen(sid, 2);

    while (1) {
	int cl = sizeof(cad);
        int nsid = accept(sid, (struct sockaddr *) &cad, &cl);
	
	signal(SIGINT, close_grace);

	while (1) {
	    read(nsid, buf, BUFLEN);

	    if(!strcmp(buf, "QUIT")) {
		close(nsid);
		close_grace(0);
	    }

	    getdata(buf, retbuf);
	
	    write(nsid, retbuf, BUFLEN);
	}
    }

    return 0;
}

