set ns [new Simulator]

$ns color 1 red
$ns color 2 blue 

set nt [open 3.nam w]
$ns namtrace-all $nt

set tr [open 3.tr w]
$ns trace-all $tr

proc finish {} {
    global ns nt tr
    $ns flush-trace
    close $nt
    close $tr
    exec nam 3.nam &
    exit 0
}

set nl [$ns node]
set nc [$ns node]
set nd [$ns node]
set nr [$ns node]

$ns duplex-link $nl $nc 20Mb 20ms DropTail
$ns duplex-link-op $nl $nc orient right

$ns duplex-link $nd $nc 10Mb 10ms DropTail
$ns duplex-link-op $nd $nc orient up 

$ns duplex-link $nr $nc 20Mb 20ms DropTail
$ns duplex-link-op $nr $nc orient left 
$ns queue-limit $nc $nd 3
$ns duplex-link-op $nc $nd queuePos 1.25

set tcpnla1 [new Agent/TCP]
$tcpnla1 set class_ 1
$tcpnla1 set fid_ 1
$ns attach-agent $nl $tcpnla1

set ftpnla1 [new Application/FTP]
$ftpnla1 set type_ FTP
$ftpnla1 attach-agent $tcpnla1

set nullnda1 [new Agent/TCPSink]
$ns attach-agent $nd $nullnda1
$ns connect $tcpnla1 $nullnda1

set udpnda1 [new Agent/UDP]
$udpnda1 set fid_ 2
$ns attach-agent $nd $udpnda1

set cbrnda1 [new Application/Traffic/CBR]
$cbrnda1 attach-agent $udpnda1
$cbrnda1 set type_ CBR
$cbrnda1 set packt_size_ 1000
$cbrnda1 set rate_ 1mb
$cbrnda1 set random_ false 

set nullnra1 [new Agent/Null]
$ns attach-agent $nr $nullnra1
$ns connect $udpnda1 $nullnra1

$ns at 0.1 "$cbrnda1 start"
$ns at 0.3 "$ftpnla1 start"

$ns at 3.0 "$cbrnda1 stop"
$ns at 4.0 "$ftpnla1 stop"

$ns at 4.0 "finish"

$ns run

