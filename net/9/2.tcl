set ns [new Simulator]

set nt [open 2.nam w]
$ns namtrace-all $nt

set tr [open 2.tr w]
$ns trace-all $tr

proc finish {} {
    global ns nt tr
    $ns flush-trace
    close $nt
    close $tr
    exec nam 2.nam &
    exit 0
}

set n0 [$ns node]
set n1 [$ns node]

$ns duplex-link $n0 $n1 10Mb 10ms DropTail

set tcp [new Agent/TCP]
$tcp set class_ 1
$ns attach-agent $n0 $tcp

set ftp [new Application/FTP]
$ftp set type FTP
$ftp attach-agent $tcp

set null [new Agent/TCPSink]
$ns attach-agent $n1 $null
$ns connect $tcp $null

$ns at 0.1 "$ftp start"
$ns at 4.5 "$ftp stop"

$ns at 5.0 "finish"

$ns run

