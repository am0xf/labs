set ns [new Simulator]

set nt [open 1.nam w]
$ns namtrace-all $nt

set tr [open 1.tr w]
$ns trace-all $tr

proc finish {} {
    global ns nt tr
    $ns flush-trace
    close $nt
    close $tr
    exec nam 1.nam &
    exit 0
}

set n0 [$ns node]
set n1 [$ns node]

$ns duplex-link $n0 $n1 10Mb 10ms DropTail

set udp [new Agent/UDP]
$ns attach-agent $n0 $udp

set cbr [new Application/Traffic/CBR]
$cbr attach-agent $udp
$cbr set type CBR
$cbr set packet_size 1000
$cbr set rate 1mb
$cbr set random false

set null [new Agent/Null]
$ns attach-agent $n1 $null
$ns connect $udp $null

$ns at 0.1 "$cbr start"
$ns at 4.5 "$cbr stop"

$ns at 5.0 "finish"

$ns run

