#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>

int main(void)
{
    int sid = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in sad, rad;
    sad.sin_family = AF_INET;
    sad.sin_addr.s_addr = inet_addr("172.16.58.36");
    sad.sin_port = htons(9734);

    int res = connect(sid, (struct sockaddr *) &sad, sizeof(sad));
    char c = 'A';
    int red = write(sid, &c, sizeof(c));

    char buf[80];
    do {
	red = read(sid, buf, sizeof(80));
	printf("%s", buf);
    } while(red != 0);

    close(sid);

    return 0;
}

